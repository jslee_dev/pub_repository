CryOri: Graphical display of crystallographic orientation
================================================================================

|

SW in this repository are for **Windows-64bit** systems, unless otherwise stated.

If you have **Runtime Error of "... dll was not found ..."**, 
please refer to the `IntelSWTools_***_redist.zip <https://gitlab.com/jslee_dev/pub_repository/-/tree/master/redist/>`_
(a collection of files from Redistributable Libraries for Intel C++ Compilers for Windows-64bit)
for the missing dll's.

|

