"""
IPF(Inverse Pole Figure) Color Map and Random Orientation

                                                                    Jae Sang Lee

램덤 방위 (Euler angles) 생성,
Euler angles -> ND, RD (miller index) 변환 (cubic symmetry),
각 방위별 IPF 표시


#-------------------------------------------------------------------------------
2024.10.12: revised set_window_title() of matplotlib
2021.07.21: "00IPF_ColorMap.py", renamed from "00random_IPF.py"
2020.09.14: Python version
"""

import math
import numpy as np
import matplotlib.pyplot as plt
import sys

PI = math.pi # 3.1415926535897932...
np.random.seed(12345)

marker_size = 2.0

#(0 0 1), (1 0 1), (1 1 1) to unit triangle R,G,B in stereographic projection
xR = 0.; yR = 0.
xG = math.sqrt(2.)-1.; yG = 0.
xB = math.sqrt(3.)/2.-1./2.; yB = xB



# Example of RGB setting for Euler angles
# ==============================================================================
def main():

    str_input = input("Type in number of random orientations (0 for a set of specific Euler angles)?: ")    
    num = int(str_input)
   
    #---------------------------------------------------------------------------
    # Set Euler angles (phi1_s, Phi_s, phi2_s), #Note! index of [1]~[num] for data
    if num == 0 :
        str_input = input("Type in phi1, Phi, phi2 (in degree): ")
        phi1 = float(str_input.split(",")[0])
        Phi = float(str_input.split(",")[1])
        phi2 = float(str_input.split(",")[2])
        print(phi1, Phi, phi2)
        # set Euler angles
        (phi1_s, Phi_s, phi2_s) = ([0.0, phi1], [0.0, Phi], [0.0, phi2]) #Note! index [0] is for dummy
    else:
        if num > 99999 or num < 0:
            print("Warning: num is too large or negative...")
            print("  Reset num = 99999\n")
            num = 99999
        # get a set of random orientations
        (phi1_s, Phi_s, phi2_s) = get_rand_Ori_Eul(num)
    #---------------------------------------------------------------------------
    # ND of the orientation, Euler angle (phi1, Phi, phi2) to (h, k, l)
    (h_s, k_s, l_s) = get_ND(phi1_s, Phi_s, phi2_s)
    # RD of the orientation, Euler angle (phi1, Phi, phi2) to (u, v, w)
    (u_s, v_s, w_s) = get_RD(phi1_s, Phi_s, phi2_s)
    #---------------------------------------------------------------------------
    # set x,y coord. in stereographic projection
    size = len(phi1_s) # size = num+1, index of [1]~[num] for data
    ND_x = np.zeros(size); ND_y = np.zeros(size)
    ND_R = np.zeros(size); ND_G = np.zeros(size); ND_B = np.zeros(size)
    RD_x = np.zeros(size); RD_y = np.zeros(size);
    RD_R = np.zeros(size); RD_G = np.zeros(size); RD_B = np.zeros(size)
    #---------------------------------------------------------------------------
    # write resuls to file
    fname_out = "random_IPF.out.txt"
    with open(fname_out, "w") as f:
        f.write("# pID,   phi1, Phi, phi2,   ND_R, ND_G, ND_B, ND_x, ND_y,   RD_R, RD_G, RD_B, RD_x, RD_y\n")
        for i in range(1, size): # size = num+1, index of [1]~[num] for data
            # (d1 d2 d3) to (X_proj, Y_proj)
            (ND_x[i], ND_y[i]) = get_XY_proj(h_s[i], k_s[i], l_s[i])
            (RD_x[i], RD_y[i]) = get_XY_proj(u_s[i], v_s[i], w_s[i])
            #(X_proj, Y_proj) to RGB
            (ND_R[i], ND_G[i], ND_B[i]) = get_RGB(ND_x[i], ND_y[i])
            (RD_R[i], RD_G[i], RD_B[i]) = get_RGB(RD_x[i], RD_y[i])
            #
            f.write("{:4d}".format(i)  
                        + ", {:13.5e}, {:13.5e}, {:13.5e}".format(phi1_s[i], Phi_s[i], phi2_s[i])
                        + ", {:13.5e}, {:13.5e}, {:13.5e}, {:13.5e}, {:13.5e}".format(ND_R[i], ND_G[i], ND_B[i], ND_x[i], ND_y[i])
                        + ", {:13.5e}, {:13.5e}, {:13.5e}, {:13.5e}, {:13.5e}".format(RD_R[i], RD_G[i], RD_B[i], RD_x[i], RD_y[i]) + "\n"
            )
            #
        #
    #
    #---------------------------------------------------------------------------
    # plot results
    plot_IPF_scatter(ND_x, ND_y, ND_R, ND_G, ND_B, RD_x, RD_y, RD_R, RD_G, RD_B)
#



# ==============================================================================
def get_ND(phi1_s, Phi_s, phi2_s):
    """
    index of [1]~[num] for data, same size(=num+1) with input lists
    """
    size = len(phi1_s)
    d1 = np.zeros(size)
    d2 = np.zeros(size)
    d3 = np.zeros(size)
    #
    for i in range(1, size):
        phi1 = phi1_s[i]; Phi = Phi_s[i]; phi2 = phi2_s[i]
        #Euler angle (phi1, Phi, phi2) to ND (d1, d2, d3)
        d1[i] = math.sin(phi2)*math.sin(Phi)
        d2[i] = math.cos(phi2)*math.sin(Phi)
        d3[i] = math.cos(Phi)
        #positive (d1 d2 d3) and d2<=d1<=d3 order for unit triangle (0 0 1),(1 0 1),(1 1 1) of cubic symmetry
        d1[i] = math.fabs(d1[i]); d2[i] = math.fabs(d2[i]); d3[i] = math.fabs(d3[i])
        if(d1[i] > d2[i]):
            tmp = d2[i]; d2[i] = d1[i]; d1[i] = tmp
        if(d2[i] > d3[i]):
            tmp = d3[i]; d3[i] = d2[i]; d2[i] = tmp
        if(d1[i] < d2[i]):
            tmp = d2[i]; d2[i] = d1[i]; d1[i] = tmp
        #
    #
    return (d1, d2, d3)
#



# ==============================================================================
def get_RD(phi1_s, Phi_s, phi2_s):
    """
    index of [1]~[num] for data, same size(=num+1) with input lists
    """
    size = len(phi1_s)
    d1 = np.zeros(size)
    d2 = np.zeros(size)
    d3 = np.zeros(size)
    #
    for i in range(1, size):
        phi1 = phi1_s[i]; Phi = Phi_s[i]; phi2 = phi2_s[i]
        #Euler angle (phi1, Phi, phi2) to RD (d1, d2, d3)
        d1[i] = math.cos(phi1)*math.cos(phi2) - math.sin(phi1)*math.sin(phi2)*math.cos(Phi)
        d2[i] = -math.cos(phi1)*math.sin(phi2) - math.sin(phi1)*math.cos(phi2)*math.cos(Phi)
        d3[i] = math.sin(phi1)*math.sin(Phi)
        #positive (d1 d2 d3) and d2<=d1<=d3 order for unit triangle (0 0 1),(1 0 1),(1 1 1) of cubic symmetry
        d1[i] = math.fabs(d1[i]); d2[i] = math.fabs(d2[i]); d3[i] = math.fabs(d3[i])
        if(d1[i] > d2[i]):
            tmp = d2[i]; d2[i] = d1[i]; d1[i] = tmp
        if(d2[i] > d3[i]):
            tmp = d3[i]; d3[i] = d2[i]; d2[i] = tmp
        if(d1[i] < d2[i]):
            tmp = d2[i]; d2[i] = d1[i]; d1[i] = tmp
        #
    #
    return (d1, d2, d3)
#



# ==============================================================================
def get_XY_proj(d1, d2, d3):
    #(d1 d2 d3) to (r=1, theta, phi), ISO convention: theta: polar angle, phi: azimuthal angle
    theta = math.acos(d3/math.sqrt(d1*d1+d2*d2+d3*d3))
    phi = 0.0 if(d1==0. and d2==0.) else math.acos(d1/math.sqrt(d1*d1+d2*d2))
    #(r=1, theta, phi) to (X_proj, Y_proj)
    X_proj = math.sin(theta)*math.cos(phi)/(1.+math.cos(theta))
    Y_proj = math.sin(theta)*math.sin(phi)/(1.+math.cos(theta))
    #
    return (X_proj, Y_proj)
#



# Ref. of IPF color map: https://mathematica.stackexchange.com/questions/47492/how-to-create-an-inverse-pole-figure-color-map
# ==============================================================================
def get_RGB(X_proj, Y_proj):
    """
    # (X_proj, Y_proj) to (R, G, B)
    # (X_proj, Y_proj) in RGB triangle (i.e. X_proj>=0.0 and Y_proj>=0.0 and Y_proj<=X_proj and (X_proj+1.)*(X_proj+1.)+Y_proj*Y_proj<=2.)
    """
    # X = 0 need special care not to divide by zero, X = 0 only for point R in RGB triangle
    if X_proj==0.0:
        val_R = 1.0
        val_G = 0.0
        val_B = 0.0
    else:
        #B_star point, the intersection of straight lines BO and RG
        x1=X_proj; y1=Y_proj; x2=xB; y2=yB; x3=xR; y3=yR; x4=xG; y4=yG
        kk=(x1-x2)*(y3-y4)-(y1-y2)*(x3-x4)
        xB_star=((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/kk
        yB_star=((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/kk
        #G_star point, the intersection of straight lines GO and RB
        x1=X_proj; y1=Y_proj; x2=xG; y2=yG; x3=xB; y3=yB; x4=xR; y4=yR
        kk=(x1-x2)*(y3-y4)-(y1-y2)*(x3-x4)
        xG_star=((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/kk
        yG_star=((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/kk
        #R_star point, the intersection of straight line RO and arc BG (a portion of (x+1)^2+y^2=2)
        kk=(Y_proj/X_proj)*(Y_proj/X_proj)
        xR_star=(math.sqrt(kk+2.)-1.)/(kk+1.)
        yR_star=Y_proj/X_proj*xR_star
        #prelimenary R,G,B values
        val_R = math.sqrt(((X_proj-xR_star)*(X_proj-xR_star)+(Y_proj-yR_star)*(Y_proj-yR_star))/((xR-xR_star)*(xR-xR_star)+(yR-yR_star)*(yR-yR_star)))
        val_G = math.sqrt(((X_proj-xG_star)*(X_proj-xG_star)+(Y_proj-yG_star)*(Y_proj-yG_star))/((xG-xG_star)*(xG-xG_star)+(yG-yG_star)*(yG-yG_star)))
        val_B = math.sqrt(((X_proj-xB_star)*(X_proj-xB_star)+(Y_proj-yB_star)*(Y_proj-yB_star))/((xB-xB_star)*(xB-xB_star)+(yB-yB_star)*(yB-yB_star)))
        #each color is normalized by max(R,G,B)
        factor = val_R if (val_R>val_G) else val_G; factor = val_B if (val_B>factor) else factor
        val_R = val_R/factor
        val_G = val_G/factor
        val_B = val_B/factor
    #
    return (val_R, val_G, val_B)
#



# ==============================================================================
def get_rand_Ori_Eul(num):
    """
    index of [1]~[num] for data
    """
    size = num + 1
    phi1_s = np.zeros(size)
    Phi_s = np.zeros(size)
    phi2_s = np.zeros(size)
    #
    for i in range(1, size): # size = num+1, index of [1]~[num] for data
        phi1_s[i] = 360.*np.random.rand()
        Phi_s[i] = math.degrees(math.acos(1.-2.*np.random.rand()))
        phi2_s[i] = 360.*np.random.rand()
    #
    return (phi1_s, Phi_s, phi2_s)



# ==============================================================================
def plot_IPF_scatter(ND_x, ND_y, ND_R, ND_G, ND_B, RD_x, RD_y, RD_R, RD_G, RD_B):
    """
    index of [1]~[num] for data, same size(=num+1) with input lists
    """
    import matplotlib.pyplot as plt
    #
    # for border lines of unit triangle
    xGB_arc = np.zeros(30) #var init
    yGB_arc = np.linspace(yG, yB, 30)
    for i, y in enumerate(yGB_arc):
        xGB_arc[i] = math.sqrt(2.-y*y) - 1.
    #
    size = len(ND_x)
    # Note: c in scatter() should not be a single numeric RGB or RGBA sequence
    # because that is indistinguishable from an array of values to be colormapped.
    ND_RGB = np.empty((size, 1), dtype=object) #a 2-D array in which the rows are RGB or RGBA, for c(colors) in scatter()
    RD_RGB = np.empty((size, 1), dtype=object)
    for i in range(1, size): # index of [1]~ for data
        ND_RGB[i][0] = (ND_R[i], ND_G[i], ND_B[i])
        RD_RGB[i][0] = (RD_R[i], RD_G[i], RD_B[i])
    #
    fig, axs = plt.subplots(1, 2)
    #
    fig.canvas.manager.set_window_title("My Window Title")
    fig.suptitle("IPF of a Set of Random Orientations") #xxx fontsize=16
    #
    ax = axs[0]
    ax.set_title("ND")
    ax.set_xlabel("x-label")
    ax.set_ylabel("y-label")
    ax.plot([xR, xG], [yR, yG], c=(0.5, 0.5, 0.5), linewidth=1.0) #line RG
    ax.plot([xR, xB], [yR, yB], c=(0.5, 0.5, 0.5), linewidth=1.0) #line RB
    ax.plot(xGB_arc, yGB_arc, c=(0.5, 0.5, 0.5), linewidth=1.0)   #arc GB
    ax.scatter(ND_x[1:], ND_y[1:], s=marker_size, c=ND_RGB[1:,0], label="ND") # index of [1]~ for data
    ax.axis([0.0, 0.5, 0.0, 0.5])
    ax.axis("square") #xxx ax.axis("equal")
    #xxx ax.legend()
    ax.axis("off")
    ax.text(-0.05, -0.03, "0 0 1")
    ax.text( 0.38, -0.03, "1 0 1")
    ax.text( 0.33,  0.38, "1 1 1")
    #
    ax = axs[1]
    ax.set_title("RD")
    ax.set_xlabel("x-label")
    ax.set_ylabel("y-label")
    ax.plot([xR, xG], [yR, yG], c=(0.5, 0.5, 0.5), linewidth=1.0) #line RG
    ax.plot([xR, xB], [yR, yB], c=(0.5, 0.5, 0.5), linewidth=1.0) #line RB
    ax.plot(xGB_arc, yGB_arc, c=(0.5, 0.5, 0.5), linewidth=1.0)   #arc GB
    ax.scatter(RD_x[1:], RD_y[1:], s=marker_size, c=RD_RGB[1:,0], label="RD") # index of [1]~ for data
    ax.axis([0.0, 0.5, 0.0, 0.5])
    ax.axis("square") #xxx ax.axis("equal")
    #xxx ax.legend()
    ax.axis("off")
    ax.text(-0.05, -0.03, "0 0 1")
    ax.text( 0.38, -0.03, "1 0 1")
    ax.text( 0.33,  0.38, "1 1 1")
    #
    plt.show()
#



# ==============================================================================
if __name__ == "__main__":
    main()
#