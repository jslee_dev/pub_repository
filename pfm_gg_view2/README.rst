pfm_gg_view2: Graphic Viewer for Grain Growth of a 3D Phase-Field Model
================================================================================
Jae Sang Lee, 2021.07,
2020.09, 2020.01,
Since 2014.07.07, 2014.03.13

|

SW in this repository are for **Windows-64bit** systems, unless otherwise stated.

|

Some example data files of polycrstalline structures for "pfm_gg_view2" are available `HERE <https://drive.google.com/drive/u/1/folders/1PWClHYLrQPi8VZkQeUF5Szp4BvFZPBps>`_ (a public folder in google drive).
    List of files (2020.10)

- pfm_gg.phi_256x256x128_fine.out.bin (85MB)
- pfm_gg.phi_256x256x128_coarse.out.bin (57MB)
- pfm_gg.phi_512x512x128_fine.out.bin (307MB)
- pfm_gg.phi_512x512x128_coarse.out.bin (234MB)
- pfm_gg.phi_512x512x128_coarse2.out.bin (204MB)
- pfm_gg.phi_512x512x128_fine_bulk.out.bin (422MB)
- pfm_gg.phi_1024x1024x128_fine.out.bin (1.2GB)
- pfm_gg.phi_1024x1024x128_coarse.out.bin (935MB)
- ...

|

Files

- "pfm_gg_view2_Release***.zip": Executable program
    .. image:: screenshot_pfm_gg_view2.png

    .. note:: If you have **Runtime Error of "... dll was not found ..."**, 
        please install the Redistributable Libraries for Intel C++ Compilers for Windows-64bit
        `"ww_icl_redist_intel64_*.msi" and/or "ww_ifort_redist_intel64_*.msi" <https://gitlab.com/jslee_dev/pub_repository/-/tree/master/redist/>`_
        for the missing dll's.

|

- "00pfm_gg_BinFileMgr_example.py": Python code, Read/Write binary files for "pfm_gg_view2.exe"
  
  (with example binary files "3000.PHI.BIN", "3000.COM.BIN")

- "00IPF_ColorMap.py": Python code, Invers Pole Figure (IPF) color map (for cubic symmetry)
    .. image:: fig_IPF_ColorMap.png

|

History
--------------------------------------------------------------------------------

2021.11.10: pfm_gg_view2 v2021.1011g, different color for the selected grid, compatibility with large (more than 2GB) data

2021.10.12: pfm_gg_view2 v2021.1011a, lighting on cubes

2021.10.05: pfm_gg_view2 v2021.0707f, com-field plot

2021.07.21: Add "00pfm_gg_BinFileMgr_example.py" and "00IPF_ColorMap.py" (separated from the prev. zip package)

2021.07.12: v2021.0707b, Add a new format of binary file (format# 210707), Re-packaging with dll files