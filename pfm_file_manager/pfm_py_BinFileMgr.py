"""
The Python code for bin file read/write for pfm_gg_view2
                                                                    Jae Sang Lee
                                                                Since 2021.07.21

- 바이너리 파일을 읽어 변수에 저장, 읽어온 변수 처리 예 등, 전역변수와 main() 함수의 주석 참조.
- 바이너리 파일 형식은 아래 표 참조. (예제 바이너리 파일: "3000.PHI.BIN", "3000.COM.BIN")

Binary Data Format# 210707a: phase field, com field

(data type, size in byte) for phase field (e.g., phi[i,j,k,p])
+-----------------------------------+-----------------------------------+-----------------------------------+
| header1 (format id)               | header2 (field type)              | header3                           |
| (int,4) 210707                    | (int,4) 100 (phi field)           | (int,4) (reserved)                |
+-----------------------------------+-----------------------------------+-----------------------------------+
| (int,4) (total number of phases)  | (int,4) (total number of grains)  | (int,4) (id of vapor phase)       |
+-----------------------------------+-----------------------------------+-----------------------------------+
| (int,4) iM                        | (int,4) jM                        | (int,4) kM                        |
+-----------------------------------+-----------------------------------+-----------------------------------+
|                                                                                                           |
|  for i = 1~iM, j = 1~jM, k = 1~kM                                                                         |
|      (char,1) pM[i,j,k] (number of phases in i,j,k grid)                                                  |
|      for p = 1~pM[i,j,k]                                                                                  |
|          (int,4) xxxxxnnnnn                                                                               |
|                                                                                                           |
+-----------------------------------------------------------------------------------------------------------+

- (data type, size in byte) for composition field (e.g., com[i,j,k,c])
+-----------------------------------+-----------------------------------+-----------------------------------+
| header1 (format id)               | header2 (field type)              | header3                           |
| (int,4) 210707                    | (int,4) 200 (com field)           | (int,4) (reserved)                |
+-----------------------------------+-----------------------------------+-----------------------------------+
| (int,4) (number of components)    | (int,4) (reserved)                | (int,4) (reserved)                |
+-----------------------------------+-----------------------------------+-----------------------------------+
| (int,4) iM                        | (int,4) jM                        | (int,4) kM                        |
+-----------------------------------+-----------------------------------+-----------------------------------+
|  for c = 1~(number of components)                                                                         |
|      (float,4) com_mean[c] (mean composition of each component)                                           |
+-----------------------------------------------------------------------------------------------------------+
|                                                                                                           |
|  for i = 1~iM, j = 1~jM, k = 1~kM                                                                         |
|      for c = 1~(number of components)                                                                     |
|          (float,4) com[i,j,k,c]                                                                           |
|                                                                                                           |
+-----------------------------------------------------------------------------------------------------------+

# ------------------------------------------------------------------------------
2021.07.28: "pfm_py_BinFileMgr.py", Renamed from 00pfm_gg_BinFileMgr_example.py
2021.07.21: Major Rev. for format# 210707a and more demonstrations
2021.07.07: "00pfm_gg_BinFileMgr_example.py", Renamed from 00pfm_gg_BinFileReadWrite.py (of old pfm_gg_view2_*2020.1021a)
2020.09.10: Python version, phi field
"""

import numpy as np
import sys


#// pre-defined in BinFileMgr (Do not change!) <<-------------------------------
ID_FORMAT210707 = 210707
ID_FORMAT150615 = 150615
ID_FORMAT_PHI = 100
ID_FORMAT_COM = 200
ID_NONE = -1
OPT_HEADERS = 0
OPT_DATA    = 1
SIZE_int   = 4
SIZE_char  = 1
SIZE_float = 4
#// pre-defined in BinFileMgr (Do not change!) >>-------------------------------
pM_alloc = 15


#// global variables for data <<------------------------------------------------
id_bin_format = 0; id_field_type = 0; header3 = 0 #var init
pIDM_init = 0; gM_init = 0; pVapor = 0 #var init
cM = 0 #var init
iM = 0; jM = 0; kM = 0 #var init
pM0ijk = np.array([]);  pID0ijkp = np.array([]);    phi3DSA0 = np.array([])     #phi field to read
pMijk = np.array([]);   pIDijkp = np.array([]);     phi3DSA = np.array([])      #phi field to write
com0_mean = np.array([]);   com3D0 = np.array([]);                              #com field to read
com_mean = np.array([]);    com3D = np.array([])                                #com field to write
"""/*
주의! 
    pM0ijk[], pID0ijkp[], phi3DSA0[], com0_mean[], com3D0[]: PFM_read_*() 가 파일로부터 설정하는 변수
    pMijk[],  pIDijkp[],  phi3DSA[],  com_mean[],  com3D[]:  PFM_write_*() 가 파일로 작성하는 변수
*/"""
#// global variables for data >>------------------------------------------------


#//=============================================================================
def main():
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global pMijk; global pIDijkp; global phi3DSA # int ***pMijk, ***pIDPijk; float ****phi3DSA;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    global com_mean; global com3D # float *com_mean, ****com3D;

    # (1) Read data from a bin file
    # --------------------------------------------------------------------------
    filename_in = input("Input bin file name (phi or com field): ")
    print("PFM_read_bin_file() with \"{}\"...".format(filename_in))
    PFM_read_bin_file(filename_in, OPT_DATA) # OPT_DATA to read data, OPT_HEADERS to read headers only
    print("PFM_read_bin_file() with \"{}\"...OK!".format(filename_in))
    # PFM_read_bin_file() 에서 id_bin_format 과 id_field_type 로부터 phi 또는 com field 등 감지.
    # 이후 phi field, com field, 옛 형식(format# 150615) 등을 구분하여 작업.

    # i,j,k of grid for monitering
    i_mon = int(0.5*iM); j_mon = int(0.5*jM); k_mon = int(0.5*kM)

    # phi field 인 경우 data 처리 예
    # --------------------------------------------------------------------------
    if(id_field_type == ID_FORMAT_PHI):
    # (2) Show some data
    # --------------------------------------------------------------------------
        print("id_bin_format, id_field_type, header3: {:d}, {:d}, {:d}".format(id_bin_format, id_field_type, header3))
        print("pIDM_init, gM_init, pVapor: {:d}, {:d}, {:d}".format(pIDM_init, gM_init, pVapor))
        print("iM, jM, kM: {:4d}, {:4d}, {:4d}".format(iM, jM, kM))
        # set a specific point and its data 
        i = i_mon; j = j_mon; k = k_mon
        pM = pM0ijk[i,j,k]
        print("  i, j, k: {:4d}, {:4d}, {:4d}".format(i, j, k))
        print("  pM: {:d}".format(pM))
        for p in range(1, pM+1): # p = 1~pM
            print("  p, pID0ijkp[i,j,k,p], phi3DSA0[i,j,k,p]: {:2d}, {:6d}, {:13.5e}".format
                 (p, pID0ijkp[i,j,k,p], phi3DSA0[i,j,k,p])
            )
        #
    # (3) Make a cross section data of 2D matrix form, example of phase map
    # --------------------------------------------------------------------------
        str_input = input("Make a data file of a cross section? (Y/N): ")
        if str_input.upper() == "Y":
            k_section = int(0.5*kM)
            filename_out = filename_in.split(".bin")[0] + ".k{:d}section.dat".format(k_section)
            file = open(filename_out, "w")
            #
            for i in range(1,iM+1):
                for j in range(1,jM+1):
                    # 각 grid에서 최대값을 갖는 pID 찾아서 기록
                    max_tmp = 0.0 #var init
                    pID = None
                    k = k_section
                    pM = pM0ijk[i,j,k]
                    for p in range(1, pM+1):
                        if (max_tmp < phi3DSA0[i,j,k,p]):
                            max_tmp = phi3DSA0[i,j,k,p]
                            pID =   pID0ijkp[i,j,k,p]
                        #
                    #
                    file.write("{:4d} ".format(pID))
                #
                file.write("\n")
            #
            file.close()
            print("Writing file \"{}\"...OK!".format(filename_out))
        #

    # (4) Make some modification with the data and write the data to a new bin file
    # --------------------------------------------------------------------------
        str_input = input("Make a data file of spherical specimen? (Y/N): ")
        if str_input.upper() == "Y":
            # copy field data to variables to write
            pMijk = pM0ijk.copy(); pIDijkp = pID0ijkp.copy(); phi3DSA = phi3DSA0.copy()
            # setting spherical region <<---------------------------------------
            # only vapor phase is assigned outside of a sphere. 
            i0 = int(0.5*iM); j0 = int(0.5*jM); k0 = int(0.5*kM)
            iradius = 10
            for i in range(1,iM+1):
                for j in range(1,jM+1):
                    for k in range(1, kM+1):
                        if((i-i0)*(i-i0)+(j-j0)*(j-j0)+(k-k0)*(k-k0) > iradius*iradius):
                            pMijk[i,j,k] = 1
                            p = 1
                            pIDijkp[i,j,k,p] = pVapor
                            phi3DSA[i,j,k,p] = 1.0
                    #
                #
            # setting spherical region >>---------------------------------------
            # ------------------------------------------------------------------
            # write a bin file
            filename_out = filename_in.split(".bin")[0] + ".new.bin"
            print("PFM_write_bin_file() with \"{}\"...".format(filename_out))
            PFM_write_bin_file(filename_out, ID_FORMAT_PHI)
            print("PFM_write_bin_file() with \"{}\"...OK!".format(filename_out))
        #
    #
    # com field 인 경우 data 처리 예
    # --------------------------------------------------------------------------
    elif(id_field_type == ID_FORMAT_COM):
        # Show some data
        # ----------------------------------------------------------------------
        print("id_bin_format, id_field_type, header3: {:d}, {:d}, {:d}".format(id_bin_format, id_field_type, header3))
        print("cM: {:d}".format(cM))
        print("iM, jM, kM: {:4d}, {:4d}, {:4d}".format(iM, jM, kM))
        # set a specific point and its data 
        i = i_mon; j = j_mon; k = k_mon
        print("i, j, k: {:4d}, {:4d}, {:4d}".format(i, j, k))
        for c in range(1, cM+1):  # c = 1~cM
            print("  c, com3D0[i,j,k,c]: {:2d}, {:13.5e}".format(c, com3D0[i,j,k,c]))

        # Make a cross section data of 2D matrix form
        # ----------------------------------------------------------------------
        str_input = input("Make a data file of a cross section? (Y/N): ")
        if str_input.upper() == "Y":
            k_section = int(0.5*kM)
            filename_out = filename_in.split(".bin")[0] + ".k{:d}section.dat".format(k_section)
            file = open(filename_out, "w")
            #
            for i in range(1,iM+1):
                for j in range(1,jM+1):
                    k = k_section
                    c = 1 # 여기서는 첫번째 component 기록
                    file.write("{:13.5e} ".format(com3D0[i,j,k,c]))
                #
                file.write("\n")
            #
            file.close()
            print("Writing file \"{}\"...OK!".format(filename_out))
        #
        # Write a bin file of com field
        # ------------------------------------------------------------------
        com_mean = com0_mean.copy(); com3D = com3D0.copy() # variables to write, copy field data
        # Make some modification  here...
        filename_out = filename_in.split(".bin")[0] + ".new.bin"
        print("PFM_write_bin_file() with \"{}\"...".format(filename_out))
        PFM_write_bin_file(filename_out, ID_FORMAT_COM)
        print("PFM_write_bin_file() with \"{}\"...OK!".format(filename_out))
    #
    # 옛 형식(format# 150615)인 경우 phi field 처리 예
    # --------------------------------------------------------------------------
    elif(id_bin_format == ID_FORMAT150615):
        # Show some data
        # ----------------------------------------------------------------------
        print("id_bin_format, id_field_type, header3: {:d}, {:d}, {:d}".format(id_bin_format, id_field_type, header3))
        print("pIDM_init, gM_init: {:d}, {:d}".format(pIDM_init, gM_init))
        print("iM, jM, kM: {:4d}, {:4d}, {:4d}".format(iM, jM, kM))
        # set a specific point and its data 
        i = i_mon; j = j_mon; k = k_mon
        pM = pM0ijk[i,j,k]
        print("  i, j, k: {:4d}, {:4d}, {:4d}".format(i, j, k))
        print("  pM: {:d}".format(pM))
        for p in range(1, pM+1): # p = 1~pM
            print("  p, pID0ijkp[i,j,k,p], phi3DSA0[i,j,k,p]: {:2d}, {:6d}, {:13.5e}".format
                 (p, pID0ijkp[i,j,k,p], phi3DSA0[i,j,k,p])
            )

        # Convert format# 150615 to format# 2100707
        # ----------------------------------------------------------------------
        # 생략, pfm_py_BinFileMgr.cpp 참조
    #

    sys.exit("End of program...")
#


#//=============================================================================
def PFM_read_bin_file(filename, option):
    """
    Read binary data file of phi or com field
    """
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global pMijk; global pIDijkp; global phi3DSA # int ***pMijk, ***pIDPijk; float ****phi3DSA;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    global com_mean; global com3D # float *com_mean, ****com3D;
    # --------------------------------------------------------------------------
    file = open(filename, 'rb')
    bytes_buffer = bytearray(file.read())
    size_buffer = len(bytes_buffer) #total bytes of file
    file.close()
    # --------------------------------------------------------------------------
    size_done = 0; size_add = 0 #var init
    size_done = size_done+size_add; size_add = SIZE_int
    id_bin_format = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True) # byteorder='little' 로 설정!
    size_done = size_done+size_add; size_add = SIZE_int
    id_field_type = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    header3 = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    # --------------------------------------------------------------------------
    # check bin format and read data for each format and field type
    if((id_bin_format == ID_FORMAT210707) and (id_field_type == ID_FORMAT_PHI or id_field_type == ID_FORMAT_COM)):
        if(id_field_type == ID_FORMAT_PHI):
            PFM_read_buffer_phi(bytes_buffer, option)
        elif(id_field_type == ID_FORMAT_COM):
            PFM_read_buffer_com(bytes_buffer, option)
    elif(id_bin_format == ID_FORMAT150615):
        # 옛 형식(format# 150615)은 phi field 코드만 작성
        PFM_read_buffer_phi_150615(bytes_buffer, option)
    else:
        print("Error! Wrong format# of bin file...")
        print("  (Available id_bin_format: {:d}, {:d}, Available id_field_type: {:d} for phi or {:d} for com field)".format
              (ID_FORMAT210707, ID_FORMAT150615, ID_FORMAT_PHI, ID_FORMAT_COM))
        print("  id_bin_format, id_field_type, header3: ", id_bin_format, id_field_type, header3)
        sys.exit("Program aborted...")
    #
#


#//=============================================================================
def PFM_read_buffer_phi(bytes_buffer, option):
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global pMijk; global pIDijkp; global phi3DSA # int ***pMijk, ***pIDPijk; float ****phi3DSA;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    global com_mean; global com3D # float *com_mean, ****com3D;
    # --------------------------------------------------------------------------
    size_buffer = len(bytes_buffer) #total bytes of file
    # --------------------------------------------------------------------------
    size_done = 0; size_add = 0 #var init
    size_done = size_done+size_add; size_add = SIZE_int
    id_bin_format = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)   # byteorder='little' 로 설정!
    size_done = size_done+size_add; size_add = SIZE_int
    id_field_type = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    header3 = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    # --------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int
    pIDM_init = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    gM_init = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    pVapor = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    iM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    jM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    kM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    # --------------------------------------------------------------------------
    if(option == OPT_HEADERS): return
    #
    pM0ijk = np.zeros((iM+1, jM+1, kM+1), dtype=np.int32)
    pID0ijkp = np.zeros((iM+1, jM+1, kM+1, pM_alloc+1), dtype=np.int32)
    phi3DSA0 = np.zeros((iM+1, jM+1, kM+1, pM_alloc+1), dtype=np.float32)
    # --------------------------------------------------------------------------
    for i in range(1, iM+1):
        for j in range(1, jM+1):
            for k in range(1, kM+1):
                size_done = size_done+size_add; size_add = SIZE_char
                pM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
                pM0ijk[i,j,k] = pM
                for p in range(1, pM+1): # pID and phi from pppppiiiii
                    size_done = size_done+size_add; size_add = SIZE_int
                    ival = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
                    pID0ijkp[i,j,k,p] = ival - int(int(ival/100000)*100000)
                    phi3DSA0[i,j,k,p] = ((float)(ival/100000))/10000
                #
            #
        #
        if(i%10 == 0): print("i/iM = {:d}/{:d}, {:d} bytes done...".format(i, iM, size_done+size_add))
    #
    size_done = size_done+size_add
    # --------------------------------------------------------------------------
    print("size of input buffer = {:d} bytes".format(size_buffer))
    print("size of data processed = {:d} bytes".format(size_done))
#


#//=============================================================================
def PFM_read_buffer_phi_150615(bytes_buffer, option): #Format 150615
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global pMijk; global pIDijkp; global phi3DSA # int ***pMijk, ***pIDPijk; float ****phi3DSA;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    global com_mean; global com3D # float *com_mean, ****com3D;
    # --------------------------------------------------------------------------
    size_buffer = len(bytes_buffer) #total bytes of file
    # --------------------------------------------------------------------------
    size_done = 0; size_add = 0 #var init
    size_done = size_done+size_add; size_add = SIZE_int
    id_bin_format = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True) # byteorder='little' 로 설정!
    size_done = size_done+size_add; size_add = SIZE_int
    id_field_type = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    header3 = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    # --------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int
    pIDM_init = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    gM_init = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    # Format 150615 는 pVapor 데이타 없음
    size_done = size_done+size_add; size_add = SIZE_int
    iM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    jM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    kM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    # --------------------------------------------------------------------------
    if(option == OPT_HEADERS): return
    #
    pM0ijk = np.zeros((iM+1, jM+1, kM+1), dtype=int)
    pID0ijkp = np.zeros((iM+1, jM+1, kM+1, pM_alloc+1), dtype=int)
    phi3DSA0 = np.zeros((iM+1, jM+1, kM+1, pM_alloc+1), dtype=float)
    # --------------------------------------------------------------------------
    for i in range(1, iM+1):
        for j in range(1, jM+1):
            for k in range(1, kM+1):
                size_done = size_done+size_add; size_add = SIZE_char
                pM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
                pM0ijk[i,j,k] = pM
                for p in range(1, pM+1): # pID and phi from pppppiiiii
                    size_done = size_done+size_add; size_add = SIZE_int
                    ival = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
                    pID0ijkp[i,j,k,p] = ival - int(int(ival/100000)*100000)
                    phi3DSA0[i,j,k,p] = ((float)(ival/100000))/10000
                #
            #
        #
        if(i%10 == 0): print("i/iM = {:d}/{:d}, {:d} bytes done...".format(i, iM, size_done+size_add))
    #
    size_done = size_done+size_add
    # --------------------------------------------------------------------------
    print("size of input buffer = {:d} bytes".format(size_buffer))
    print("size of data processed = {:d} bytes".format(size_done))
#


#//=============================================================================
def PFM_read_buffer_com(bytes_buffer, option):
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global pMijk; global pIDijkp; global phi3DSA # int ***pMijk, ***pIDPijk; float ****phi3DSA;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    global com_mean; global com3D # float *com_mean, ****com3D;
    # --------------------------------------------------------------------------
    size_buffer = len(bytes_buffer) #total bytes of file
    # --------------------------------------------------------------------------
    size_done = 0; size_add = 0 #var init
    size_done = size_done+size_add; size_add = SIZE_int
    id_bin_format = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True) # byteorder='little' 로 설정!
    size_done = size_done+size_add; size_add = SIZE_int
    id_field_type = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    header3 = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    # --------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int
    cM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    idummy = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    idummy = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    iM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    jM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    kM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    # --------------------------------------------------------------------------
    if(option == OPT_HEADERS): return
    #
    com0_mean = np.zeros(cM+1, dtype=np.float32)
    com3D0 = np.zeros((iM+1, jM+1, kM+1, cM+1), dtype=np.float32)
    # --------------------------------------------------------------------------
    for c in range(1, cM+1):
        size_done = size_done+size_add; size_add = SIZE_float
        # interpret 4 bytes as a 32-bit float using Python
        bytes_tmp = np.array(bytes_buffer[size_done:size_done+size_add], dtype=np.uint8)
        com0_mean[c] = bytes_tmp.view(dtype=np.float32)
    #
    for i in range(1, iM+1):
        for j in range(1, jM+1):
            for k in range(1, kM+1):
                for c in range(1, cM+1):
                    size_done = size_done+size_add; size_add = SIZE_float
                    # interpret 4 bytes as a 32-bit float using Python
                    bytes_tmp = np.array(bytes_buffer[size_done:size_done+size_add], dtype=np.uint8)
                    com3D0[i,j,k,c] = bytes_tmp.view(dtype=np.float32)
                #
            #
        #
        if(i%10 == 0): print("i/iM = {:d}/{:d}, {:d} bytes done...".format(i, iM, size_done+size_add))
    #
    size_done = size_done+size_add
    # --------------------------------------------------------------------------
    print("size of input buffer = {:d} bytes".format(size_buffer))
    print("size of data processed = {:d} bytes".format(size_done))
#


#//=============================================================================
def PFM_write_bin_file(filename, id):
    """
    Write binary data file of phi or com field
    """
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global pMijk; global pIDijkp; global phi3DSA # int ***pMijk, ***pIDPijk; float ****phi3DSA;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    global com_mean; global com3D # float *com_mean, ****com3D;
    # --------------------------------------------------------------------------
    if(id == ID_FORMAT_PHI):
        (bytes_buffer, size_buffer) = PFM_write_buffer_phi()
    elif(id == ID_FORMAT_COM):
        (bytes_buffer, size_buffer) = PFM_write_buffer_com()
    #
    file = open(filename, 'wb')
    file.write(bytes_buffer)
    file.close()
    # --------------------------------------------------------------------------
#


#//=============================================================================
def PFM_write_buffer_phi():
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global pMijk; global pIDijkp; global phi3DSA # int ***pMijk, ***pIDPijk; float ****phi3DSA;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    global com_mean; global com3D # float *com_mean, ****com3D;
    # --------------------------------------------------------------------------
    id_bin_format = ID_FORMAT210707; id_field_type = ID_FORMAT_PHI
    bytes_buffer =bytearray([]) #var init
    # --------------------------------------------------------------------------
    size_done = 0; size_add = 0 #var init
    size_done = size_done+size_add; size_add = SIZE_int
    ival = id_bin_format; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = id_field_type; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = header3; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    # --------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int
    ival =pIDM_init; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = gM_init; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = pVapor; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = iM; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = jM; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = kM; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    # --------------------------------------------------------------------------
    for i in range(1,iM+1):
        for j in range(1,jM+1):
            for k in range(1, kM+1):
                pM = pMijk[i,j,k]
                size_done = size_done+size_add; size_add = SIZE_char
                ival = int(pM); bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True)) #Note! bytearray1.extend(bytearray2) is much faster than bytearray1 + bytearray2.
                for p in range(1, pM+1):
                    phi_and_pID = int(phi3DSA[i,j,k,p]*10000)*int(100000) + int(pIDijkp[i,j,k,p]) #(pppppiiiii) 형태로 저장
                    size_done = size_done+size_add; size_add = SIZE_int
                    ival = int(phi_and_pID); bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True)) #Note! bytearray1.extend(bytearray2) is much faster than bytearray1 + bytearray2.
                #
            #
        #
        if(i%10 == 0): print("i/iM = {:d}/{:d}, {:d} bytes done...".format(i, iM, size_done+size_add))
    #
    size_done = size_done+size_add
    # --------------------------------------------------------------------------
    size_buffer = len(bytes_buffer) #total bytes of file
    print("size of data processed = {:d} bytes".format(size_done))
    print("size of output buffer = {:d} bytes".format(size_buffer))

    return (bytes_buffer, size_buffer)
#


#//=============================================================================
def PFM_write_buffer_com():
    idummy = ID_NONE
    #
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global pMijk; global pIDijkp; global phi3DSA # int ***pMijk, ***pIDPijk; float ****phi3DSA;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    global com_mean; global com3D # float *com_mean, ****com3D;
    # --------------------------------------------------------------------------
    id_bin_format = ID_FORMAT210707; 
    id_field_type = ID_FORMAT_COM; 
    # --------------------------------------------------------------------------
    bytes_buffer =bytearray([]) #var init
    # --------------------------------------------------------------------------
    size_done = 0; size_add = 0 #var init
    size_done = size_done+size_add; size_add = SIZE_int
    ival = id_bin_format; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = id_field_type; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = header3; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    # --------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int
    ival =cM; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = idummy; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = idummy; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = iM; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = jM; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    size_done = size_done+size_add; size_add = SIZE_int
    ival = kM; bytes_buffer.extend(ival.to_bytes(size_add, "little", signed=True))
    # --------------------------------------------------------------------------
    for c in range(1, cM+1):
        size_done = size_done+size_add; size_add = SIZE_float
        # float to bytes
        fval = com_mean[c]; 
        bytes_tmp = np.array([fval], dtype=np.float32).tobytes()
        bytes_buffer.extend(bytes_tmp)
    #
    for i in range(1,iM+1):
        for j in range(1,jM+1):
            for k in range(1, kM+1):
                 for c in range(1, cM+1):
                    size_done = size_done+size_add; size_add = SIZE_float
                    # float to bytes
                    fval = com3D[i,j,k,c]; 
                    bytes_tmp = np.array([fval], dtype=np.float32).tobytes()
                    bytes_buffer.extend(bytes_tmp)
                #
            #
        #
        if(i%10 == 0): print("i/iM = {:d}/{:d}, {:d} bytes done...".format(i, iM, size_done+size_add))
    #
    size_done = size_done+size_add
    # --------------------------------------------------------------------------
    size_buffer = len(bytes_buffer) #total bytes of file
    print("size of data processed = {:d} bytes".format(size_done))
    print("size of output buffer = {:d} bytes".format(size_buffer))

    return (bytes_buffer, size_buffer)
#


#//=============================================================================
if __name__ == "__main__":
    main()
#



