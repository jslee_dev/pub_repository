"""
This code merges .PHI.BIN and .COM.BIN files to an ascii .VTK file.
Please refer to "pfm_py_BinFileMgr.py" (2021.07.28, Jae Sang Lee) for detailed data format.

                                                                                Jiwon Park
                                                                                Aug 6, 2021

    .VTK viewer: ParaView (https://www.paraview.org)

"""

import numpy as np
import sys
import glob

# pre-defined setting (Do not change!) <<---------------------------------------
ID_FORMAT210707 = 210707; ID_FORMAT_PHI = 100; ID_FORMAT_COM = 200
ID_FORMAT150615 = 150615
pM_alloc = 15; SIZE_int = 4; SIZE_char = 1; SIZE_short = 2; SIZE_float = 4
# pre-defined setting (Do not change!) >>---------------------------------------

# global variables for data <<--------------------------------------------------
id_bin_format = 0; id_field_type = 0; header3 = 0 #var init
pIDM_init = 0; gM_init = 0; pVapor = 0 #var init
cM = 0 #var init
iM = 0; jM = 0; kM = 0 #var init
pM0ijk = np.array([])
pID0ijkp = np.array([])
phi3DSA0 = np.array([])
com0_mean = np.array([])
com3D0 = np.array([])
# global variables for data >>--------------------------------------------------

# ==============================================================================
def main():

    # collect .bin files
    phifiles = glob.glob("*.PHI.BIN")
    comfiles = glob.glob("*.COM.BIN")

    n = 0
    while n < len(phifiles):
        phiNNNN = phifiles[n]
        comNNNN = comfiles[n]
        phiNNNN_splt = phiNNNN.split(".")
        comNNNN_splt = comNNNN.split(".")
        if phiNNNN_splt[0] != comNNNN_splt[0]:
            print("Missing \"{}.BIN\" file!".format(phiNNNN_splt[0]))
            break
        else:
            pass

        # read .PHI.BIN
        PFM_read_bin_file(phiNNNN)
        if (id_field_type == ID_FORMAT_PHI):
            filename_vtk = "Result_"+phiNNNN_splt[0]+".vtk"
            filevtk = open(filename_vtk,'w')

            # write vtk header
            filevtk.write("# vtk DataFile Version 3.0 \n"
                          "MPFM\nASCII\nDATASET STRUCTURED_POINTS\n")
            filevtk.write("DIMENSIONS " + str(iM) + " " + str(jM) + ' ' + str(kM) + '\n')
            filevtk.write("ORIGIN 0 0 0\n")
            filevtk.write("SPACING 1.00e-05 1.00e-05 1.00e-05\n\n")  # need to be linked to Module Input_data

            # write pID
            filevtk.write("POINT_DATA " + str(iM * jM * kM) + '\n')
            filevtk.write("SCALARS pID int 1\n")
            filevtk.write("LOOKUP_TABLE default\n")
            for i in range(1, iM+1):
                for j in range(1, jM+1):
                    for k in range(1, kM+1):
                        max_tmp = 0.0
                        pID = None
                        pM = pM0ijk[i,j,k]
                        for p in range(1, pM+1):
                            if (max_tmp < phi3DSA0[i][j][k][p]):
                                max_tmp = phi3DSA0[i][j][k][p]
                                pID = pID0ijkp[i,j,k,p]
                        filevtk.write("%d\n" % pID)
                        #filevtk.write(str(pID)+'\n')
                if(i%10 == 0): print("Writing pID i/iM = {:d}/{:d} done...".format(i, iM))
            filevtk.close()
            print("Writing pID in %s.VTK is completed!" % phiNNNN_splt[0])
        else:
            break

        # read .COM.BIN
        PFM_read_bin_file(comNNNN)
        if (id_field_type == ID_FORMAT_COM):
            filevtk = open(filename_vtk, 'a')

            # write Comp
            for c in range(1, cM+1):
                filevtk.write("SCALARS Comp%d double 1\n" % c)
                filevtk.write("LOOKUP_TABLE default\n")
                for i in range(1, iM + 1):
                    for j in range(1, jM + 1):
                        for k in range(1, kM + 1):
                            #comp = com3D0[i,j,k,c]
                            #filevtk.write(str(comp)+'\n')
                            filevtk.write("{:.5f}\n".format(com3D0[i,j,k,c]))
                    if (i % 10 == 0): print("Writing Comp i/iM = {:d}/{:d} in c/cM = {:d}/{:d} done...".format(i, iM, c, cM))
                filevtk.write("\n")
            filevtk.close()
            print("Writing Comp in %s.VTK is completed!" % phiNNNN_splt[0])
        else:
            break
        n += 1


def PFM_read_bin_file(filename):
    """
    Read binary data file of phi or com field
    """
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    #---------------------------------------------------------------------------
    file = open(filename, 'rb')
    bytes_buffer = bytearray(file.read())
    size_buffer = len(bytes_buffer) #total bytes of file
    file.close()
    #---------------------------------------------------------------------------
    size_done = 0; size_add = 0 #var init
    size_done = size_done+size_add; size_add = SIZE_int
    id_bin_format = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True) # byteorder='little' 로 설정!
    size_done = size_done+size_add; size_add = SIZE_int
    id_field_type = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    header3 = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    # check bin format and read data for each format and field type
    if((id_bin_format == ID_FORMAT210707) and (id_field_type == ID_FORMAT_PHI or id_field_type == ID_FORMAT_COM)):
        if(id_field_type == ID_FORMAT_PHI):
            PFM_read_buffer_phi(bytes_buffer)
        elif(id_field_type == ID_FORMAT_COM):
            PFM_read_buffer_com(bytes_buffer)
    elif(id_bin_format == ID_FORMAT150615):
        # 여기서는 옛 형식(format# 150615)은 phi field 코드만 작성
        PFM_read_buffer_phi_150615(bytes_buffer)
    else:
        print("Error! Wrong format# of bin file...")
        print("  (Available id_bin_format: {:d}, {:d}, Available id_field_type: {:d} for phi or {:d} for com field)".format
              (ID_FORMAT210707, ID_FORMAT150615, ID_FORMAT_PHI, ID_FORMAT_COM))
        print("  id_bin_format, id_field_type, header3: ", id_bin_format, id_field_type, header3)
        sys.exit("Program aborted...")
    #
#

# ==============================================================================
def PFM_read_buffer_phi(bytes_buffer):
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    #---------------------------------------------------------------------------
    size_buffer = len(bytes_buffer) #total bytes of file
    #---------------------------------------------------------------------------
    size_done = 0; size_add = 0 #var init
    size_done = size_done+size_add; size_add = SIZE_int
    id_bin_format = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True) # byteorder='little' 로 설정!
    size_done = size_done+size_add; size_add = SIZE_int
    id_field_type = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    header3 = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    #---------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int
    pIDM_init = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    gM_init = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    pVapor = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    iM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    jM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    kM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    #---------------------------------------------------------------------------
    pM0ijk = np.zeros((iM+1, jM+1, kM+1), dtype=np.int32)
    pID0ijkp = np.zeros((iM+1, jM+1, kM+1, pM_alloc+1), dtype=np.int32)
    phi3DSA0 = np.zeros((iM+1, jM+1, kM+1, pM_alloc+1), dtype=np.float32)
    #---------------------------------------------------------------------------
    for i in range(1, iM+1):
        for j in range(1, jM+1):
            for k in range(1, kM+1):
                size_done = size_done+size_add; size_add = SIZE_char
                pM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
                pM0ijk[i,j,k] = pM
                for p in range(1, pM+1):
                    size_done = size_done+size_add; size_add = SIZE_int
                    ival = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
                    pID0ijkp[i,j,k,p] = ival - int(int(ival/100000)*100000)
                    phi3DSA0[i,j,k,p] = ((float)(ival/100000))/10000
                #
            #
        #
        if(i%10 == 0): print("i/iM = {:d}/{:d}, {:d} bytes done...".format(i, iM, size_done+size_add))
    #
    size_done = size_done+size_add
    #---------------------------------------------------------------------------
    print("size of input buffer = {:d} bytes".format(size_buffer))
    print("size of data processed = {:d} bytes".format(size_done))
#

# ==============================================================================
def PFM_read_buffer_com(bytes_buffer):
    # global variables for data
    global id_bin_format; global id_field_type; global header3 # int id_bin_format, id_field_type, header3;
    global pIDM_init; global gM_init; global pVapor # int pIDM_init, gM_init, pVapor;
    global cM
    global iM; global jM; global kM # int iM, jM, kM;
    global pM0ijk; global pID0ijkp; global phi3DSA0 # int ***pM0ijk, ***pIDP0ijk; float ****phi3DSA0;
    global com0_mean; global com3D0 # float *com0_mean, ****com3D0;
    #---------------------------------------------------------------------------
    size_buffer = len(bytes_buffer) #total bytes of file
    #---------------------------------------------------------------------------
    size_done = 0; size_add = 0 #var init
    size_done = size_done+size_add; size_add = SIZE_int
    id_bin_format = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True) # byteorder='little' 로 설정!
    size_done = size_done+size_add; size_add = SIZE_int
    id_field_type = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    header3 = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    #---------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int
    cM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    reserved = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    reserved = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    iM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    jM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    size_done = size_done+size_add; size_add = SIZE_int
    kM = int.from_bytes(bytes_buffer[size_done:size_done+size_add], "little", signed=True)
    #---------------------------------------------------------------------------
    com0_mean = np.zeros(cM+1, dtype=np.float32)
    com3D0 = np.zeros((iM+1, jM+1, kM+1, cM+1), dtype=np.float32)
    #---------------------------------------------------------------------------
    for c in range(1, cM+1):
        size_done = size_done+size_add; size_add = SIZE_float
        # interpret 4 bytes as a 32-bit float using Python
        bytes_tmp = np.array(bytes_buffer[size_done:size_done+size_add], dtype=np.uint8)
        com0_mean[c] = bytes_tmp.view(dtype=np.float32)
    #
    for i in range(1, iM+1):
        for j in range(1, jM+1):
            for k in range(1, kM+1):
                for c in range(1, cM+1):
                    size_done = size_done+size_add; size_add = SIZE_float
                    # interpret 4 bytes as a 32-bit float using Python
                    bytes_tmp = np.array(bytes_buffer[size_done:size_done+size_add], dtype=np.uint8)
                    com3D0[i,j,k,c] = bytes_tmp.view(dtype=np.float32)
                #
            #
        #
        if(i%10 == 0): print("i/iM = {:d}/{:d}, {:d} bytes done...".format(i, iM, size_done+size_add))
    #
    size_done = size_done+size_add
    #---------------------------------------------------------------------------
    print("size of input buffer = {:d} bytes".format(size_buffer))
    print("size of data processed = {:d} bytes".format(size_done))
#



#===========================================================================
if __name__ == "__main__":
    main()
#

