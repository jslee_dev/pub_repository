/*
"pfm_aux_BinFileMgr.cpp" (WIP)

                                                  Jae Sang Lee, Since 2015.06.15

This program is provided \"as is\" without warranty of any kind.

bin_file 포맷
  - Format# 210707a: assigne header2 = field type, add pVapor
  - Format# 150615: int header1, header2, header3 추가, 변수 pM 크기 char형 (pre150614 에서는  short형)
  - Format pre150614: old



// old remark <<----------------------------------------------------------------
PFM_GG Binary File Manager (150907 파일명 변경 from "pfm_aux_BinFileConv.c")
프로그램 기능
  Opt_pIDReset: pID Arrangement, IC_*FromFile 용 bin 파일의 pIDM_init 감소를 위해 필요(150905)
  Opt_InfoView: Header 정보, grid system 표시
  Opt_Conv: Format "pre150614 --> Format "150615" 변환

150915: pID reset 후 phi값 없는 일부 pID 잔존하는 오류 수정
// old remark >>----------------------------------------------------------------
*/

#define Code_Ver  "PFM Binary File Manager(WIP) " //Code Version
#include <iostream>
#include <string>
#include <io.h>       //access() 사용


#define I_OK (1)
#define I_ERR_read_bin_file         -10000
#define I_ERR_read_buffer_phi       -10100
#define I_ERR_read_buffer_com       -10200
#define I_ERR_write_bin_file        -11000
#define I_ERR_write_buffer_phi      -11100
#define I_ERR_write_buffer_com      -11200



//임시 <<---------------------------------------
//pM_Max, pIDM_Ext, MaxStrLenName 등 공유
#define pM_Max 15 //(150526)In 3D IC_NucGrow에서는 pM_Max=10 사용시 몇개의 grid에서만 pM초과 발생, IC_Poly 초기 결정립 형성단계에서는 pM_Max=10 사용시 pM초과 발생(pM>30, 강제로 pM감소시켜 계산진행하면 ntime경과하면서 조직은 형성됨) cf: In 2D, IC_general 로 초반 조직 생성시 pM_Max 15(10은 오류 발생)
#define pIDM_Ext 5
#define MaxStrLenName 32
#define MaxStrLenDummy 256

#define pUndef -1

int pIDM_alloc;

float *phisumpID;

enum {Opt_None, Opt_InfoView, Opt_pIDReset, Opt_Conv};

void PFM_read_bin_phi(char *fname, int option);
void BinFileConv_pre150614_to_150615(char *fname_bin_old, char *fname_bin_new);
void PFM_set_phisumpID(void);
void pID_reset(char *fname); //150907, since 150905
//임시 >>---------------------------------------


//임시, NR functions <<--------------------------
#define NR_END 1
#define FREE_ARG char*

void nrerror(char error_text[]);
float *vector(long nl, long nh);
double *dvector(long nl, long nh);
int *ivector(long nl, long nh);

unsigned char *cvector(long nl, long nh);
unsigned long *lvector(long nl, long nh);

float **matrix(long nrl, long nrh, long ncl, long nch);
double **dmatrix(long nrl, long nrh, long ncl, long nch);
int **imatrix(long nrl, long nrh, long ncl, long nch);

float **submatrix(float **a, long oldrl, long oldrh, long oldcl, long oldch,
	long newrl, long newcl);
float **convert_matrix(float *a, long nrl, long nrh, long ncl, long nch);

float ***f3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh);
double ***d3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh);//added by JSLee
int ***i3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh);//added by JSLee

float ****f4array(long nrl, long nrh, long ncl, long nch, long ndl, long ndh, long n4l, long n4h);//added by JSLee
double ****d4array(long nrl, long nrh, long ncl, long nch, long ndl, long ndh, long n4l, long n4h);//added by JSLee
int ****i4array(long nrl, long nrh, long ncl, long nch, long ndl, long ndh, long n4l, long n4h);//added by JSLee

void free_vector(float *v, long nl, long nh);
void free_dvector(double *v, long nl, long nh);
void free_ivector(int *v, long nl, long nh);

void free_cvector(unsigned char *v, long nl, long nh);
void free_lvector(unsigned long *v, long nl, long nh);

void free_matrix(float **m, long nrl, long nrh, long ncl, long nch);
void free_dmatrix(double **m,long nrl,long nrh,long ncl,long nch);
void free_imatrix(int **m, long nrl, long nrh, long ncl, long nch);

void free_submatrix(float **b, long nrl, long nrh, long ncl, long nch);
void free_convert_matrix(float **b, long nrl, long nrh, long ncl, long nch);

void free_f3tensor(float ***t, long nrl, long nrh, long ncl, long nch, long ndl, long ndh);
void free_d3tensor(double ***t, long nrl, long nrh, long ncl, long nch, long ndl, long ndh);//added by JSLee
void free_i3tensor(int ***t, long nrl, long nrh, long ncl, long nch, long ndl, long ndh);//added by JSLee

void free_f4array(float ****a, long nrl, long nrh, long ncl, long nch, long ndl, long ndh, long n4l, long n4h);//added by JSLee
void free_d4array(double ****a, long nrl, long nrh, long ncl, long nch, long ndl, long ndh, long n4l, long n4h);//added by JSLee
void free_i4array(int ****a, long nrl, long nrh, long ncl, long nch, long ndl, long ndh, long n4l, long n4h);//added by JSLee
//임시, NR functions >>--------------------------







//# pre-defined in BinFileMgr (Do not change!) <<-------------------------------
#define ID_FORMAT210707     210707
#define ID_FORMAT150615     150615
#define ID_FORMAT_PHI       100
#define ID_FORMAT_COM       200
#define ID_NONE             -1
#define OPT_HEADERS         0
#define OPT_DATA            1
#define SIZE_int            4
#define SIZE_char           1
#define SIZE_float          4
//# pre-defined in BinFileMgr (Do not change!) >>-------------------------------
int pM_alloc = 15;


//# global variables for data <<------------------------------------------------
int id_bin_format, id_field_type, header3;
int pIDM_init, gM_init, pVapor;
int cM;
int iM, jM, kM;
int ***pM0ijk, ****pID0ijkp; float ****phi3DSA0;
int ***pMijk,  ****pIDijkp;  float ****phi3DSA;
float *com0_mean, ****com3D0;
float *com_mean, ****com3D;
/*"""
주의! 
    pM0ijk[], pID0ijkp[], phi3DSA0[], com0_mean[], com3D0[]: PFM_read_*() 가 파일로부터 설정하는 변수
    pMijk[],  pIDijkp[],  phi3DSA[],  com_mean[],  com3D[]:  PFM_write_*() 가 파일로 작성하는 변수
"""*/
//# global variables for data >>------------------------------------------------


int PFM_read_bin_file(char *filename, int option);
int PFM_read_buffer_phi(char *bytes_buffer, size_t size_buffer, int option);
int PFM_read_buffer_phi_150615(char *bytes_buffer, size_t size_buffer, int option);
int PFM_read_buffer_com(char *bytes_buffer, size_t size_buffer, int option);

int PFM_write_bin_file(char *filename, int id);
int PFM_write_buffer_phi(char *bytes_buffer, size_t *psize_buffer);
int PFM_write_buffer_com(char *bytes_buffer, size_t *psize_buffer);



//#=============================================================================
int main(void)
{
    char filename_in[MaxStrLenName], filename_out[MaxStrLenName];

    // (1) Read data from a bin file
    //--------------------------------------------------------------------------
    fprintf(stdout, "Input bin file name (phi or com field): ");
    fscanf(stdin, "%s", filename_in); getchar(); //여기의 getchar() 는 scanf() 시 입력한 [Enter] 소모용
    fprintf(stdout, "PFM_read_bin_file() with \"%s\"...\n", filename_in);
    PFM_read_bin_file(filename_in, OPT_DATA); // OPT_DATA to read data, OPT_HEADERS to read headers only
    fprintf(stdout, "PFM_read_bin_file() with \"%s\"...OK!\n", filename_in);
    // PFM_read_bin_file() 에서 id_bin_format 과 id_field_type 로부터 phi 또는 com field 등 감지.
    // 이후 phi field, com field, 옛 형식(format# 150615) 등을 구분하여 작업.

    // i,j,k of grid for monitering
    int i_mon = int(0.5*iM), j_mon = int(0.5*jM), k_mon = int(0.5*kM);

    // phi field 인 경우 data 처리 예
    //--------------------------------------------------------------------------
    if(id_field_type == ID_FORMAT_PHI)
    {
    // (2) Show some data
    //--------------------------------------------------------------------------
        fprintf(stdout, "id_bin_format, id_field_type, header3: %d, %d, %d\n", id_bin_format, id_field_type, header3);
        fprintf(stdout, "pIDM_init, gM_init, pVapor: %d, %d, %d\n", pIDM_init, gM_init, pVapor);
        fprintf(stdout, "iM, jM, kM: %4d, %4d, %4d\n", iM, jM, kM);
        // set a specific point and its data
        int i = i_mon, j = j_mon, k = k_mon;
        int pM = pM0ijk[i][j][k];
        fprintf(stdout, "  i, j, k: %4d, %4d, %4d\n", i, j, k);
        fprintf(stdout, "  pM: %d\n", pM);
        for(int p = 1; p <= pM; p++)
        {
            fprintf(stdout, "  p, pID0ijkp[i,j,k,p], phi3DSA0[i,j,k,p]: %2d, %6d, %13.5e\n", 
                            p, pID0ijkp[i][j][k][p], phi3DSA0[i][j][k][p]);
        }
    // (3) Make a cross section data of 2D matrix form, example of phase map
    //--------------------------------------------------------------------------
    // 생략, pfm_py_BinFileMgr.py 참조

    // (4) Make some modification with the data and write the data to a new bin file
    //--------------------------------------------------------------------------
    // 생략, pfm_py_BinFileMgr.py 참조

    }
    // com field 인 경우 data 처리 예
    //--------------------------------------------------------------------------
    else if(id_field_type == ID_FORMAT_COM)
    {
        // Show some data
        //----------------------------------------------------------------------
        fprintf(stdout, "id_bin_format, id_field_type, header3: %d, %d, %d\n", id_bin_format, id_field_type, header3);
        fprintf(stdout, "cM: %d\n", cM);
        int i = i_mon, j = j_mon, k = k_mon;
        fprintf(stdout, "i, j, k: %4d, %4d, %4d\n", i, j, k);
        for(int c = 1; c <= cM; c++)
        {
            fprintf(stdout, "  c, com3D0[i,j,k,c]: %dd, %13.5e\n", c, com3D0[i][j][k][c]);
        }
        // Make a cross section data of 2D matrix form
        //----------------------------------------------------------------------
        // 생략, pfm_py_BinFileMgr.py 참조

        // Write a bin file of com field
        //------------------------------------------------------------------
        // 생략, pfm_py_BinFileMgr.py 참조
    }
    // 옛 형식(format# 150615)인 경우 phi field 처리 예
    //--------------------------------------------------------------------------
    else if(id_bin_format == ID_FORMAT150615)
    {
        // Show some data
        //----------------------------------------------------------------------
        fprintf(stdout, "id_bin_format, id_field_type, header3: %d, %d, %d\n", id_bin_format, id_field_type, header3);
        fprintf(stdout, "pIDM_init, gM_init: %d, %d\n", pIDM_init, gM_init);
        fprintf(stdout, "iM, jM, kM: %4d, %4d, %4d\n", iM, jM, kM);
        // set a specific point and its data
        int i = i_mon, j = j_mon, k = k_mon;
        int pM = pM0ijk[i][j][k];
        fprintf(stdout, "  i, j, k: %4d, %4d, %4d\n", i, j, k);
        fprintf(stdout, "  pM: %d\n", pM);
        for(int p = 1; p <= pM; p++)
        {
            fprintf(stdout, "  p, pID0ijkp[i,j,k,p], phi3DSA0[i,j,k,p]: %2d, %6d, %13.5e\n", p, pID0ijkp[i][j][k][p], phi3DSA0[i][j][k][p]);
        }

        // Convert format# 150615 to format# 2100707
        //----------------------------------------------------------------------
        pMijk = pM0ijk, pIDijkp = pID0ijkp, phi3DSA = phi3DSA0; //pMijk, pIDijkp, phi3DSA to write
        id_bin_format = ID_FORMAT210707, id_field_type = ID_FORMAT_PHI;  pVapor = 1; //pVapor = 1 in the program using format# 150615
        sprintf(filename_out, "%s%s", filename_in, ".new.bin");
        fprintf(stdout, "PFM_write_bin_file() with \"%s\"...\n", filename_out);
        PFM_write_bin_file(filename_out, id_field_type);
        fprintf(stdout, "PFM_write_bin_file() with \"%s\"...OK!\n", filename_out);
    }

    fprintf(stdout, "End of program...\n");
    fprintf(stdout, "Press [Enter] key to finish program.");  
    getchar();

    return I_OK;
}


// Code in pfm_aux_BinFileMgr.cpp, 210801 <<------------------------------------
//#=============================================================================
int PFM_read_bin_file(char *filename, int option)
{
    //--------------------------------------------------------------------------
    FILE *fpb_in;
    if((fpb_in = fopen(filename, "rb")) == NULL) {printf("Error! Cannot open file \"%s\"...\n", filename); printf("Input file aborted..."); return I_ERR_read_bin_file;}
    //obtain file size
    size_t size_buffer, size_read;
    fseek (fpb_in , 0 , SEEK_END); 
    size_buffer = ftell (fpb_in); 
    rewind (fpb_in);
    //allocate memory to contain the whole file and copy the file to the buffer
    char *bytes_buffer;
    bytes_buffer = (char*) malloc (SIZE_char*size_buffer); if (bytes_buffer == NULL) {printf("Memory error...\n"); return I_ERR_read_bin_file;}
    size_read = fread (bytes_buffer, 1, size_buffer, fpb_in); if (size_read != size_buffer) {printf("Reading error..."); return I_ERR_read_bin_file;}
    fclose(fpb_in);
    //--------------------------------------------------------------------------
    size_t size_done = 0, size_add = 0; //var init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&id_bin_format, bytes_buffer+size_done, size_add);     //id_bin_format
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&id_field_type, bytes_buffer+size_done, size_add);     //id_field_type
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&header3, bytes_buffer+size_done, size_add);           //header3
    //--------------------------------------------------------------------------
    // check bin format and read data for each format and field type
    if(id_bin_format == ID_FORMAT210707)
    {
        if(id_field_type == ID_FORMAT_PHI) PFM_read_buffer_phi(bytes_buffer, size_buffer, option);
        else if(id_field_type == ID_FORMAT_COM) PFM_read_buffer_com(bytes_buffer, size_buffer, option);
    }
    else if(id_bin_format == ID_FORMAT150615)
    {
        // 옛 형식(format# 150615)은 phi field 코드만 작성
        PFM_read_buffer_phi_150615(bytes_buffer, size_buffer, option);
        id_field_type = ID_FORMAT_PHI; //ID_FORMAT150615 은 field type 강제 할당 필요!
        pVapor = 1; //ID_FORMAT150615 은 pVapor = 1 강제 할당 필요!
    }
    else
    {
        fprintf(stdout, "Error! Wrong format# of bin file...\n");
        fprintf(stdout, "  (Available id_bin_format: %d, %d, Available id_field_type: %d for phi or %d for com field)",
                ID_FORMAT210707, ID_FORMAT150615, ID_FORMAT_PHI, ID_FORMAT_COM);
        fprintf(stdout, "  id_bin_format, id_field_type, header3: %d, %d, %d", id_bin_format, id_field_type, header3);
        fprintf(stdout, "Input file aborted..."); return I_ERR_read_bin_file;
    }

    return I_OK;
}



//#=============================================================================
int PFM_read_buffer_phi(char *bytes_buffer, size_t size_buffer, int option)
{
    size_t size_done = 0, size_add = 0; //var init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&id_bin_format, bytes_buffer+size_done, size_add);       //id_bin_format
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&id_field_type, bytes_buffer+size_done, size_add);       //id_field_type
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&header3, bytes_buffer+size_done, size_add);             //header3
    //--------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&pIDM_init, bytes_buffer+size_done, size_add);           //pIDM_init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&gM_init, bytes_buffer+size_done, size_add);             //pIDM_init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&pVapor, bytes_buffer+size_done, size_add);              //pVapor
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&iM, bytes_buffer+size_done, size_add);                  //iM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&jM, bytes_buffer+size_done, size_add);                  //jM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&kM, bytes_buffer+size_done, size_add);                  //kM
    //--------------------------------------------------------------------------
    if(option == OPT_HEADERS) return I_OK;
    //
    //이전에 할당된 배열이 있는 경우 해제 후, 동적 할당
    { 
        if(pM0ijk!=NULL)    free_i3tensor(pM0ijk, 0,iM+1,0,jM+1,0,kM+1);
        if(pID0ijkp!=NULL)  free_i4array(pID0ijkp, 0,iM+1,0,jM+1,0,kM+1, 0,pM_alloc);
        if(phi3DSA0!=NULL)  free_f4array(phi3DSA0, 0,iM+1,0,jM+1,0,kM+1, 0,pM_alloc);
    }
    pM0ijk = i3tensor(0,iM+1,0,jM+1,0,kM+1);
    pID0ijkp = i4array(0,iM+1,0,jM+1,0,kM+1, 0,pM_alloc);
    phi3DSA0 = f4array(0,iM+1,0,jM+1,0,kM+1, 0,pM_alloc);
    //--------------------------------------------------------------------------
    int ival;
    int i, j, k;
    char p, pMchar; 
    for(i=1; i<=iM; i++) { for(j =1; j<=jM; j++) { for(k =1; k<=kM; k++) {
        size_done = size_done+size_add; size_add = SIZE_char;
        memcpy(&pMchar, bytes_buffer+size_done, size_add); if(pMchar>pM_Max || pMchar<0){printf("Error! Wrong data in bin_file...\nPress [Enter] key to finish..."); return I_ERR_read_buffer_phi;}
        pM0ijk[i][j][k] = (int)pMchar;
        for(p = 1; p <= pMchar; p++)
        {   // pID and phi from pppppiiiii
            size_done = size_done+size_add; size_add = SIZE_int;
            memcpy(&ival, bytes_buffer+size_done, size_add);
            pID0ijkp[i][j][k][p] = ival - (ival/100000)*100000;
            phi3DSA0[i][j][k][p] = ((float)(ival/100000))/10000;
        }
    }}}
    size_done = size_done+size_add;
    //--------------------------------------------------------------------------
    fprintf(stdout, "size of input buffer = %ld bytes\n", size_buffer);
    fprintf(stdout, "size of data processed = %ld bytes\n", size_done);

    return I_OK;
}



//#=============================================================================
int PFM_read_buffer_phi_150615(char *bytes_buffer, size_t size_buffer, int option) //Format 150615
{
    size_t size_done = 0, size_add = 0; //var init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&id_bin_format, bytes_buffer+size_done, size_add);       //id_bin_format
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&id_field_type, bytes_buffer+size_done, size_add);       //id_field_type
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&header3, bytes_buffer+size_done, size_add);             //header3
    //--------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&pIDM_init, bytes_buffer+size_done, size_add);           //pIDM_init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&gM_init, bytes_buffer+size_done, size_add);             //pIDM_init
    // Format 150615 는 pVapor 데이타 없음
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&iM, bytes_buffer+size_done, size_add);                  //iM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&jM, bytes_buffer+size_done, size_add);                  //jM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&kM, bytes_buffer+size_done, size_add);                  //kM
    //--------------------------------------------------------------------------
    if(option == OPT_HEADERS) return I_OK;
    //
    //이전에 할당된 배열이 있는 경우 해제 후, 동적 할당
    { 
        if(pM0ijk!=NULL)    free_i3tensor(pM0ijk, 0,iM+1,0,jM+1,0,kM+1);
        if(pID0ijkp!=NULL)  free_i4array(pID0ijkp, 0,iM+1,0,jM+1,0,kM+1, 0,pM_alloc);
        if(phi3DSA0!=NULL)  free_f4array(phi3DSA0, 0,iM+1,0,jM+1,0,kM+1, 0,pM_alloc);
    }
    pM0ijk = i3tensor(0,iM+1,0,jM+1,0,kM+1);
    pID0ijkp = i4array(0,iM+1,0,jM+1,0,kM+1, 0,pM_alloc);
    phi3DSA0 = f4array(0,iM+1,0,jM+1,0,kM+1, 0,pM_alloc);
    //--------------------------------------------------------------------------
    int ival;
    int i, j, k;
    char p, pMchar; 
    for(i=1; i<=iM; i++) { for(j =1; j<=jM; j++) { for(k =1; k<=kM; k++) {
        size_done = size_done+size_add; size_add = SIZE_char;
        memcpy(&pMchar, bytes_buffer+size_done, size_add); if(pMchar>pM_Max || pMchar<0){printf("Error! Wrong data in bin_file...\nPress [Enter] key to finish..."); return I_ERR_read_buffer_phi;}
        pM0ijk[i][j][k] = (int)pMchar;
        for(p = 1; p <= pMchar; p++)
        {   // pID and phi from pppppiiiii
            size_done = size_done+size_add; size_add = SIZE_int;
            memcpy(&ival, bytes_buffer+size_done, size_add);
            pID0ijkp[i][j][k][p] = ival - (ival/100000)*100000;
            phi3DSA0[i][j][k][p] = ((float)(ival/100000))/10000;
        }
    }}}
    size_done = size_done+size_add;
    //--------------------------------------------------------------------------
    fprintf(stdout, "size of input buffer = %ld bytes\n", size_buffer);
    fprintf(stdout, "size of data processed = %ld bytes\n", size_done);

    return I_OK;
}



//#=============================================================================
int PFM_read_buffer_com(char *bytes_buffer, size_t size_buffer, int option)
{
    int idummy;
    //
    size_t size_done = 0, size_add = 0; //var init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&id_bin_format, bytes_buffer+size_done, size_add);       //id_bin_format
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&id_field_type, bytes_buffer+size_done, size_add);       //id_field_type
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&header3, bytes_buffer+size_done, size_add);             //header3
    //--------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&cM, bytes_buffer+size_done, size_add);                  //cM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&idummy, bytes_buffer+size_done, size_add);              //reserved
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&idummy, bytes_buffer+size_done, size_add);              //reserved
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&iM, bytes_buffer+size_done, size_add);                  //iM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&jM, bytes_buffer+size_done, size_add);                  //jM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(&kM, bytes_buffer+size_done, size_add);                  //kM
    //--------------------------------------------------------------------------
    if(option == OPT_HEADERS) return I_OK;
    //
    //이전에 할당된 배열이 있는 경우 해제 후, 동적 할당
    { 
        if(com0_mean!=NULL) free_vector(com0_mean, 0,cM);
        if(com3D0!=NULL)    free_f4array(com3D0, 0,iM+1,0,jM+1,0,kM+1, 0,cM);
    }
    com0_mean = vector(0,cM);
    com3D0 = f4array(0,iM+1,0,jM+1,0,kM+1, 0,cM);
    //--------------------------------------------------------------------------
    int i, j, k;
    int c; 
    for(c = 1; c <= cM; c++)
    {
        size_done = size_done+size_add; size_add = SIZE_float;
        memcpy(&com0_mean[c], bytes_buffer+size_done, size_add);
    }
    for(i=1; i<=iM; i++) { for(j =1; j<=jM; j++) { for(k =1; k<=kM; k++) {
        for(c = 1; c <= cM; c++)
        {
            size_done = size_done+size_add; size_add = SIZE_float;
            memcpy(&com3D0[i][j][k][c], bytes_buffer+size_done, size_add);
        }
    }}}
    size_done = size_done+size_add;
    //--------------------------------------------------------------------------
    fprintf(stdout, "size of input buffer = %ld bytes\n", size_buffer);
    fprintf(stdout, "size of data processed = %ld bytes\n", size_done);

    return I_OK;
}



//#=============================================================================
int PFM_write_bin_file(char *filename, int id)
{
    char *bytes_buffer;
    size_t size_buffer = 0; //var init

    if(id == ID_FORMAT_PHI) 
    {   //allocate memory to contain the whole file, 이후 memcpy() 하고 확정한 크기만 파일로 저장, realloc() 불필요.
        bytes_buffer = (char*) malloc (SIZE_int*(iM*jM*kM)*pM_alloc);
        if (bytes_buffer == NULL) {fprintf(stdout, "Memory allocation error...\n"); return I_ERR_write_bin_file;}
        //
        PFM_write_buffer_phi(bytes_buffer, &size_buffer);
    }
    else if(id == ID_FORMAT_COM)
    {   //allocate memory to contain the whole file, 이후 memcpy() 하고 확정한 크기만 파일로 저장, realloc() 불필요.
        bytes_buffer = (char*) malloc (SIZE_int*(iM*jM*kM)*cM);
        if (bytes_buffer == NULL) {fprintf(stdout, "Memory allocation error...\n"); return I_ERR_write_bin_file;}
        //
        PFM_write_buffer_com(bytes_buffer, &size_buffer);
    } 

    FILE *fpb_out;
    if((fpb_out = fopen(filename, "wb")) == NULL) {fprintf(stdout, "Error! Cannot open file \"%s\"...\n", filename); return I_ERR_write_bin_file;}
    fwrite(bytes_buffer, 1, size_buffer, fpb_out);
    fclose(fpb_out);

    free(bytes_buffer);

    return I_OK;
}



//#=============================================================================
int PFM_write_buffer_phi(char *bytes_buffer, size_t *psize_buffer)
{
    id_bin_format = ID_FORMAT210707; //bin file format
    id_field_type = ID_FORMAT_PHI; //field type
    header3; //reserved
    //--------------------------------------------------------------------------
    size_t size_done = 0, size_add = 0; //var init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &id_bin_format, size_add);     //id_bin_format
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &id_field_type, size_add);     //id_field_type
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &header3, size_add);           //header3
    //--------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &pIDM_init, size_add);         //pIDM_init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &gM_init, size_add);           //gM_init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &pVapor, size_add);            //pVapor
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &iM, size_add);                //iM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &jM, size_add);                //jM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &kM, size_add);                //kM
    //--------------------------------------------------------------------------
    int i, j, k;
    int ival;
    char p, pMchar;
    for(i=1; i<=iM; i++) { for(j =1; j<=jM; j++) { for(k =1; k<=kM; k++) {
        pMchar = (char)pMijk[i][j][k];
        size_done = size_done+size_add; size_add = SIZE_char;
        memcpy(bytes_buffer+size_done, &pMchar, size_add);        //pM[i,j,k]
        for(p = 1; p <= pMchar; p++)
        {
            ival=((int)(phi3DSA[i][j][k][p]*10000))*100000 + pIDijkp[i][j][k][p]; //pppppiiiii
            size_done = size_done+size_add; size_add = SIZE_int;
            memcpy(bytes_buffer+size_done, &ival, size_add);
        }
    }}}
    size_done = size_done+size_add;
    //--------------------------------------------------------------------------
    *psize_buffer = size_done;
    fprintf(stdout, "size of data processed = %ld bytes\n", size_done);

    return I_OK;
}



//#=============================================================================
int PFM_write_buffer_com(char *bytes_buffer, size_t *psize_buffer)
{
    int idummy = ID_NONE;
    //
    id_bin_format = ID_FORMAT210707; //bin file format
    id_field_type = ID_FORMAT_COM; //field type
    header3; //reserved
    //--------------------------------------------------------------------------
    size_t size_done = 0, size_add = 0; //var init
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &id_bin_format, size_add);     //id_bin_format
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &id_field_type, size_add);     //id_field_type
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &header3, size_add);           //header3
    //--------------------------------------------------------------------------
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &cM, size_add);                //cM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &idummy, size_add);            //reserved
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &idummy, size_add);            //reserved
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &iM, size_add);                //iM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &jM, size_add);                //jM
    size_done = size_done+size_add; size_add = SIZE_int;
    memcpy(bytes_buffer+size_done, &kM, size_add);                //kM
    //--------------------------------------------------------------------------
    int i, j, k;
    int c;
    for(c = 1; c <= cM; c++)
    {
        size_done = size_done+size_add; size_add = SIZE_float;
        memcpy(bytes_buffer+size_done, &com_mean[c], size_add);
    }
    for(i=1; i<=iM; i++) { for(j =1; j<=jM; j++) { for(k =1; k<=kM; k++) {
        for(c = 1; c <= cM; c++)
        {
            size_done = size_done+size_add; size_add = SIZE_float;
            memcpy(bytes_buffer+size_done, &com3D[i][j][k][c], size_add);
        }
    }}}
    size_done = size_done+size_add;
    //--------------------------------------------------------------------------
    *psize_buffer = size_done;
    fprintf(stdout, "size of data processed = %ld bytes\n", size_done);

    return I_OK;
}
// Code in pfm_aux_BinFileMgr.cpp, 210801 >>------------------------------------

//from "pfm_gg_vars.cpp"
//Functions for memory allocation <<--------------------------------------------
//200925: Rev. for a larger number of grids, e.g. 1024*1024*128: long --> long long for malloc args
//140309: Adapted from NR in C v2.11

void nrerror(char error_text[])
/* Numerical Recipes standard error handler */
{
	fprintf(stderr,"Numerical Recipes run-time error...\n");
	fprintf(stderr,"%s\n",error_text);
	fprintf(stderr,"...now exiting to system...\n");
	exit(1);
}

float *vector(long nl, long nh)
/* allocate a float vector with subscript range v[nl..nh] */
{
	float *v;

	v=(float *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(float)));
	if (!v) nrerror("allocation failure in vector()");
	return v-nl+NR_END;
}

int *ivector(long nl, long nh)
/* allocate an int vector with subscript range v[nl..nh] */
{
	int *v;

	v=(int *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(int)));
	if (!v) nrerror("allocation failure in ivector()");
	return v-nl+NR_END;
}

double *dvector(long nl, long nh)
/* allocate a double vector with subscript range v[nl..nh] */
{
	double *v;

	v=(double *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(double)));
	if (!v) nrerror("allocation failure in dvector()");
	return v-nl+NR_END;
}

unsigned char *cvector(long nl, long nh)
/* allocate an unsigned char vector with subscript range v[nl..nh] */
{
	unsigned char *v;

	v=(unsigned char *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(unsigned char)));
	if (!v) nrerror("allocation failure in cvector()");
	return v-nl+NR_END;
}

unsigned long *lvector(long nl, long nh)
/* allocate an unsigned long vector with subscript range v[nl..nh] */
{
	unsigned long *v;

	v=(unsigned long *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(long)));
	if (!v) nrerror("allocation failure in lvector()");
	return v-nl+NR_END;
}

float **matrix(long nrl, long nrh, long ncl, long nch)
/* allocate a float matrix with subscript range m[nrl..nrh][ncl..nch] */
{
	long long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	float **m;

	/* allocate pointers to rows */
	m=(float **) malloc((size_t)((nrow+NR_END)*sizeof(float*)));
	if (!m) nrerror("allocation failure 1 in matrix()");
	m += NR_END;
	m -= nrl;

	/* allocate rows and set pointers to them */
	m[nrl]=(float *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(float)));
	if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
	m[nrl] += NR_END;
	m[nrl] -= ncl;

	for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

	/* return pointer to array of pointers to rows */
	return m;
}

double **dmatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a double matrix with subscript range m[nrl..nrh][ncl..nch] */
{
	long long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	double **m;

	/* allocate pointers to rows */
	m=(double **) malloc((size_t)((nrow+NR_END)*sizeof(double*)));
	if (!m) nrerror("allocation failure 1 in matrix()");
	m += NR_END;
	m -= nrl;

	/* allocate rows and set pointers to them */
	m[nrl]=(double *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double)));
	if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
	m[nrl] += NR_END;
	m[nrl] -= ncl;

	for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

	/* return pointer to array of pointers to rows */
	return m;
}

int **imatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a int matrix with subscript range m[nrl..nrh][ncl..nch] */
{
	long long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	int **m;

	/* allocate pointers to rows */
	m=(int **) malloc((size_t)((nrow+NR_END)*sizeof(int*)));
	if (!m) nrerror("allocation failure 1 in matrix()");
	m += NR_END;
	m -= nrl;


	/* allocate rows and set pointers to them */
	m[nrl]=(int *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(int)));
	if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
	m[nrl] += NR_END;
	m[nrl] -= ncl;

	for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

	/* return pointer to array of pointers to rows */
	return m;
}

float **submatrix(float **a, long oldrl, long oldrh, long oldcl, long oldch,
	long newrl, long newcl)
/* point a submatrix [newrl..][newcl..] to a[oldrl..oldrh][oldcl..oldch] */
{
	long long i,j,nrow=oldrh-oldrl+1,ncol=oldcl-newcl;
	float **m;

	/* allocate array of pointers to rows */
	m=(float **) malloc((size_t) ((nrow+NR_END)*sizeof(float*)));
	if (!m) nrerror("allocation failure in submatrix()");
	m += NR_END;
	m -= newrl;

	/* set pointers to rows */
	for(i=oldrl,j=newrl;i<=oldrh;i++,j++) m[j]=a[i]+ncol;

	/* return pointer to array of pointers to rows */
	return m;
}

float **convert_matrix(float *a, long nrl, long nrh, long ncl, long nch)
/* allocate a float matrix m[nrl..nrh][ncl..nch] that points to the matrix
declared in the standard C manner as a[nrow][ncol], where nrow=nrh-nrl+1
and ncol=nch-ncl+1. The routine should be called with the address
&a[0][0] as the first argument. */
{
	long long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1;
	float **m;

	/* allocate pointers to rows */
	m=(float **) malloc((size_t) ((nrow+NR_END)*sizeof(float*)));
	if (!m) nrerror("allocation failure in convert_matrix()");
	m += NR_END;
	m -= nrl;

	/* set pointers to rows */
	m[nrl]=a-ncl;
	for(i=1,j=nrl+1;i<nrow;i++,j++) m[j]=m[j-1]+ncol;
	/* return pointer to array of pointers to rows */
	return m;
}

float ***f3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh)
/* allocate a float 3tensor with range t[nrl..nrh][ncl..nch][ndl..ndh] */
{
	long long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1;
	float ***t;

	/* allocate pointers to pointers to rows */
	t=(float ***) malloc((size_t)((nrow+NR_END)*sizeof(float**)));
	if (!t) nrerror("allocation failure 1 in f3tensor()");
	t += NR_END;
	t -= nrl;

	/* allocate pointers to rows and set pointers to them */
	t[nrl]=(float **) malloc((size_t)((nrow*ncol+NR_END)*sizeof(float*)));
	if (!t[nrl]) nrerror("allocation failure 2 in f3tensor()");
	t[nrl] += NR_END;
	t[nrl] -= ncl;

	/* allocate rows and set pointers to them */
	t[nrl][ncl]=(float *) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(float)));
	if (!t[nrl][ncl]) nrerror("allocation failure 3 in f3tensor()");
	t[nrl][ncl] += NR_END;
	t[nrl][ncl] -= ndl;

	for(j=ncl+1;j<=nch;j++) t[nrl][j]=t[nrl][j-1]+ndep;
	for(i=nrl+1;i<=nrh;i++) {
		t[i]=t[i-1]+ncol;
		t[i][ncl]=t[i-1][ncl]+ncol*ndep;
		for(j=ncl+1;j<=nch;j++) t[i][j]=t[i][j-1]+ndep;
	}

	/* return pointer to array of pointers to rows */
	return t;
}

double ***d3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh)
/* allocate a double d3tensor with range t[nrl..nrh][ncl..nch][ndl..ndh] */
// Adapted from f3tensor() by JSLee
{
        long long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1;
        double ***t;

        /* allocate pointers to pointers to rows */
        t=(double ***) malloc((size_t)((nrow+NR_END)*sizeof(double**)));
        if (!t) nrerror("allocation failure 1 in d3tensor()");
        t += NR_END;
        t -= nrl;

        /* allocate pointers to rows and set pointers to them */
        t[nrl]=(double **) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double*)));
        if (!t[nrl]) nrerror("allocation failure 2 in d3tensor()");
        t[nrl] += NR_END;
        t[nrl] -= ncl;

        /* allocate rows and set pointers to them */
        t[nrl][ncl]=(double *) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(double)));
        if (!t[nrl][ncl]) nrerror("allocation failure 3 in d3tensor()");
        t[nrl][ncl] += NR_END;
        t[nrl][ncl] -= ndl;

        for(j=ncl+1;j<=nch;j++) t[nrl][j]=t[nrl][j-1]+ndep;
        for(i=nrl+1;i<=nrh;i++) {
                t[i]=t[i-1]+ncol;
                t[i][ncl]=t[i-1][ncl]+ncol*ndep;
                for(j=ncl+1;j<=nch;j++) t[i][j]=t[i][j-1]+ndep;
        }

        /* return pointer to array of pointers to rows */
        return t;
}

int ***i3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh)
/* allocate a int i3tensor with range t[nrl..nrh][ncl..nch][ndl..ndh] */
// Adapted from f3tensor() by JSLee
{
	long long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1;
	int ***t;

	/* allocate pointers to pointers to rows */
	t=(int ***) malloc((size_t)((nrow+NR_END)*sizeof(int**)));
	if (!t) nrerror("allocation failure 1 in i3tensor()");
	t += NR_END;
	t -= nrl;

	/* allocate pointers to rows and set pointers to them */
	t[nrl]=(int **) malloc((size_t)((nrow*ncol+NR_END)*sizeof(int*)));
	if (!t[nrl]) nrerror("allocation failure 2 in i3tensor()");
	t[nrl] += NR_END;
	t[nrl] -= ncl;

	/* allocate rows and set pointers to them */
	t[nrl][ncl]=(int *) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(int)));
	if (!t[nrl][ncl]) nrerror("allocation failure 3 in i3tensor()");
	t[nrl][ncl] += NR_END;
	t[nrl][ncl] -= ndl;

	for(j=ncl+1;j<=nch;j++) t[nrl][j]=t[nrl][j-1]+ndep;
	for(i=nrl+1;i<=nrh;i++) {
		t[i]=t[i-1]+ncol;
		t[i][ncl]=t[i-1][ncl]+ncol*ndep;
		for(j=ncl+1;j<=nch;j++) t[i][j]=t[i][j-1]+ndep;
	}

	/* return pointer to array of pointers to rows */
	return t;
}

float ****f4array(long nrl, long nrh, long ncl, long nch, long ndl, long ndh, long n4l, long n4h)
//added by JSLee
{
	long long i,j,k,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1,n4=n4h-n4l+1;
	float ****a;

	a=(float ****) malloc((size_t)((nrow+NR_END)*sizeof(float***)));
	if (!a) nrerror("allocation failure 1 in f4arrary()");
	a += NR_END;
	a -= nrl;

	a[nrl]=(float ***) malloc((size_t)((nrow*ncol+NR_END)*sizeof(float**)));
	if (!a[nrl]) nrerror("allocation failure 2 in f4arrary()");
	a[nrl] += NR_END;
	a[nrl] -= ncl;

	a[nrl][ncl]=(float **) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(float*)));
	if (!a[nrl][ncl]) nrerror("allocation failure 3 in f4arrary()");
	a[nrl][ncl] += NR_END;
	a[nrl][ncl] -= ndl;

	a[nrl][ncl][ndl]=(float *) malloc((size_t)((nrow*ncol*ndep*n4+NR_END)*sizeof(float)));
	if (!a[nrl][ncl][ndl]) nrerror("allocation failure 4 in f4arrary()");
	a[nrl][ncl][ndl] += NR_END;
	a[nrl][ncl][ndl] -= ndl;


	for(j=ncl+1;j<=nch;j++) { 
		a[nrl][j]=a[nrl][j-1]+ndep;//a[nrl][ncl]-->a[nrl][j]
		a[nrl][j][ndl]=a[nrl][j-1][ndl]+ndep*n4;
		for(k=ndl+1;k<=ndh;k++) {
			a[nrl][j][k]=a[nrl][j][k-1]+n4;
		}
	}
	for(k=ndl+1;k<=ndh;k++) {
		a[nrl][ncl][k]=a[nrl][ncl][k-1]+n4;	//112 오류해소
	}
	
	//loop_ijk 
    for(i=nrl+1;i<=nrh;i++) {
        a[i] = a[i-1]+ncol;//a[nrl]-->a[i]
		a[i][ncl] = a[i-1][ncl]+ncol*ndep;//시작점 a[i][ncl] 설정
		a[i][ncl][ndl] = a[i-1][ncl][ndl]+ncol*ndep*n4; 	//211 오류해소
        for(j=ncl+1;j<=nch;j++) {
            a[i][j] = a[i][j-1]+ndep;//a[i][j] from a[i][ncl]
			a[i][j][ndl] = a[i][j-1][ndl]+ndep*n4;//시작점 a[i][j][ndl] 설정
            for(k=ndl+1;k<=ndh;k++) {
                a[i][j][k] = a[i][j][k-1]+n4;//a[i][j][k] from a[i][j][ndl]
            }
        }
    }

    for(i=nrl+1;i<=nrh;i++) {
		for(k=ndl+1;k<=ndh;k++) {
			a[i][ncl][k]=a[i][ncl][k-1]+n4;	 //212 오류해소, 위의 loop_ijk 다음에 설정해야함...
		}
	}

	return a;
}

double ****d4array(long nrl, long nrh, long ncl, long nch, long ndl, long ndh, long n4l, long n4h)
//added by JSLee, adapted from f4array()
{
	long long i,j,k,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1,n4=n4h-n4l+1;
	double ****a;

	a=(double ****) malloc((size_t)((nrow+NR_END)*sizeof(double***)));
	if (!a) nrerror("allocation failure 1 in d4arrary()");
	a += NR_END;
	a -= nrl;

	a[nrl]=(double ***) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double**)));
	if (!a[nrl]) nrerror("allocation failure 2 in d4arrary()");
	a[nrl] += NR_END;
	a[nrl] -= ncl;

	a[nrl][ncl]=(double **) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(double*)));
	if (!a[nrl][ncl]) nrerror("allocation failure 3 in d4arrary()");
	a[nrl][ncl] += NR_END;
	a[nrl][ncl] -= ndl;

	a[nrl][ncl][ndl]=(double *) malloc((size_t)((nrow*ncol*ndep*n4+NR_END)*sizeof(double)));
	if (!a[nrl][ncl][ndl]) nrerror("allocation failure 4 in d4arrary()");
	a[nrl][ncl][ndl] += NR_END;
	a[nrl][ncl][ndl] -= ndl;


	for(j=ncl+1;j<=nch;j++) { 
		a[nrl][j]=a[nrl][j-1]+ndep;
		a[nrl][j][ndl]=a[nrl][j-1][ndl]+ndep*n4;
		for(k=ndl+1;k<=ndh;k++) {
			a[nrl][j][k]=a[nrl][j][k-1]+n4;
		}
	}
	for(k=ndl+1;k<=ndh;k++) {
		a[nrl][ncl][k]=a[nrl][ncl][k-1]+n4;
	}
	
	//loop_ijk 
    for(i=nrl+1;i<=nrh;i++) {
        a[i] = a[i-1]+ncol;
		a[i][ncl] = a[i-1][ncl]+ncol*ndep;
		a[i][ncl][ndl] = a[i-1][ncl][ndl]+ncol*ndep*n4;
        for(j=ncl+1;j<=nch;j++) {
            a[i][j] = a[i][j-1]+ndep;
			a[i][j][ndl] = a[i][j-1][ndl]+ndep*n4;
            for(k=ndl+1;k<=ndh;k++) {
                a[i][j][k] = a[i][j][k-1]+n4;
            }
        }
    }

    for(i=nrl+1;i<=nrh;i++) {
		for(k=ndl+1;k<=ndh;k++) {
			a[i][ncl][k]=a[i][ncl][k-1]+n4;	 
		}
	}

	return a;
}

int ****i4array(long nrl, long nrh, long ncl, long nch, long ndl, long ndh, long n4l, long n4h)
//added by JSLee, adapted from f4array()
{
	long long i,j,k,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1,n4=n4h-n4l+1;
	int ****a;

	a=(int ****) malloc((size_t)((nrow+NR_END)*sizeof(int***)));
	if (!a) nrerror("allocation failure 1 in i4arrary()");
	a += NR_END;
	a -= nrl;

	a[nrl]=(int ***) malloc((size_t)((nrow*ncol+NR_END)*sizeof(int**)));
	if (!a[nrl]) nrerror("allocation failure 2 in i4arrary()");
	a[nrl] += NR_END;
	a[nrl] -= ncl;

	a[nrl][ncl]=(int **) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(int*)));
	if (!a[nrl][ncl]) nrerror("allocation failure 3 in i4arrary()");
	a[nrl][ncl] += NR_END;
	a[nrl][ncl] -= ndl;

	a[nrl][ncl][ndl]=(int *) malloc((size_t)((nrow*ncol*ndep*n4+NR_END)*sizeof(int)));
	if (!a[nrl][ncl][ndl]) nrerror("allocation failure 4 in i4arrary()");
	a[nrl][ncl][ndl] += NR_END;
	a[nrl][ncl][ndl] -= ndl;


	for(j=ncl+1;j<=nch;j++) { 
		a[nrl][j]=a[nrl][j-1]+ndep;
		a[nrl][j][ndl]=a[nrl][j-1][ndl]+ndep*n4;
		for(k=ndl+1;k<=ndh;k++) {
			a[nrl][j][k]=a[nrl][j][k-1]+n4;
		}
	}
	for(k=ndl+1;k<=ndh;k++) {
		a[nrl][ncl][k]=a[nrl][ncl][k-1]+n4;
	}
	
	//loop_ijk 
    for(i=nrl+1;i<=nrh;i++) {
        a[i] = a[i-1]+ncol;
		a[i][ncl] = a[i-1][ncl]+ncol*ndep;
		a[i][ncl][ndl] = a[i-1][ncl][ndl]+ncol*ndep*n4;
        for(j=ncl+1;j<=nch;j++) {
            a[i][j] = a[i][j-1]+ndep;
			a[i][j][ndl] = a[i][j-1][ndl]+ndep*n4;
            for(k=ndl+1;k<=ndh;k++) {
                a[i][j][k] = a[i][j][k-1]+n4;
            }
        }
    }

    for(i=nrl+1;i<=nrh;i++) {
		for(k=ndl+1;k<=ndh;k++) {
			a[i][ncl][k]=a[i][ncl][k-1]+n4;	 
		}
	}

	return a;
}

void free_vector(float *v, long nl, long nh)
/* free a float vector allocated with vector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_dvector(double *v, long nl, long nh)
/* free a double vector allocated with dvector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_ivector(int *v, long nl, long nh)
/* free an int vector allocated with ivector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_cvector(unsigned char *v, long nl, long nh)
/* free an unsigned char vector allocated with cvector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_lvector(unsigned long *v, long nl, long nh)
/* free an unsigned long vector allocated with lvector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

void free_matrix(float **m, long nrl, long nrh, long ncl, long nch)
/* free a float matrix allocated by matrix() */
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}

void free_dmatrix(double **m, long nrl, long nrh, long ncl, long nch)
/* free a double matrix allocated by dmatrix() */
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}

void free_imatrix(int **m, long nrl, long nrh, long ncl, long nch)
/* free an int matrix allocated by imatrix() */
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}

void free_submatrix(float **b, long nrl, long nrh, long ncl, long nch)
/* free a submatrix allocated by submatrix() */
{
	free((FREE_ARG) (b+nrl-NR_END));
}

void free_convert_matrix(float **b, long nrl, long nrh, long ncl, long nch)
/* free a matrix allocated by convert_matrix() */
{
	free((FREE_ARG) (b+nrl-NR_END));
}


void free_f3tensor(float ***t, long nrl, long nrh, long ncl, long nch,
	long ndl, long ndh)
/* free a float f3tensor allocated by f3tensor() */
{
	free((FREE_ARG) (t[nrl][ncl]+ndl-NR_END));
	free((FREE_ARG) (t[nrl]+ncl-NR_END));
	free((FREE_ARG) (t+nrl-NR_END));
}

void free_d3tensor(double ***t, long nrl, long nrh, long ncl, long nch,
        long ndl, long ndh)
/* free a double d3tensor allocated by d3tensor() */
// Adapted from free_f3tensor() by JSLee
{
        free((char*) (t[nrl][ncl]+ndl-NR_END));
        free((char*) (t[nrl]+ncl-NR_END));
        free((char*) (t+nrl-NR_END));
}

void free_i3tensor(int ***t, long nrl, long nrh, long ncl, long nch,
	long ndl, long ndh)
/* free a float f3tensor allocated by f3tensor() */
// Adapted from free_f3tensor() by JSLee
{
	free((FREE_ARG) (t[nrl][ncl]+ndl-NR_END));
	free((FREE_ARG) (t[nrl]+ncl-NR_END));
	free((FREE_ARG) (t+nrl-NR_END));
}

void free_f4array(float ****a, long nrl, long nrh, long ncl, long nch,
	long ndl, long ndh, long n4l, long n4h)
//added by JSLee
{
	free((FREE_ARG) (a[nrl][ncl][ndl]+n4l-NR_END));
	free((FREE_ARG) (a[nrl][ncl]+ndl-NR_END));
	free((FREE_ARG) (a[nrl]+ncl-NR_END));
	free((FREE_ARG) (a+nrl-NR_END));
}

void free_d4array(double ****a, long nrl, long nrh, long ncl, long nch,
	long ndl, long ndh, long n4l, long n4h)
//added by JSLee
{
	free((FREE_ARG) (a[nrl][ncl][ndl]+n4l-NR_END));
	free((FREE_ARG) (a[nrl][ncl]+ndl-NR_END));
	free((FREE_ARG) (a[nrl]+ncl-NR_END));
	free((FREE_ARG) (a+nrl-NR_END));
}

void free_i4array(int ****a, long nrl, long nrh, long ncl, long nch,
	long ndl, long ndh, long n4l, long n4h)
//added by JSLee
{
	free((FREE_ARG) (a[nrl][ncl][ndl]+n4l-NR_END));
	free((FREE_ARG) (a[nrl][ncl]+ndl-NR_END));
	free((FREE_ARG) (a[nrl]+ncl-NR_END));
	free((FREE_ARG) (a+nrl-NR_END));
}

//Functions for memory allocation >>--------------------------------------------