!===============================================================================
! Binary file input and output for "pfm_gg_view2.exe"
! Example Code in FORTRAN
!                                                          Jae Sang Lee, POSTECH
!                                                                           2021
!                                        Kyung Min Kim and Jae Sang Lee, POSTECH
!                                                                     2015.08.20
!
! xxxxxnnnnn(10자리 정수) 형태로 binary 파일 쓰기, 읽기 
!   phi*10000 = xxxxx (=0~10000) = (0.0, 0.0001 ~ 0.9999, 1.0)*10000
!   pID       = nnnnn (=0~99999)
! phi, pID --> ival (write bin_file 경우) : ival = ((int)(phi*10000))*100000 + pID
! ival --> phi, pID (read bin_file 경우) : phi = ((float)(ival/100000))/10000., pID = ival - (ival/100000)*100000
! i,j,k-loop 순서: i{j{k{}}}
!
! NCOEXIST(I,J,K) = I,J,K grid에서 공존하는 phase 개수
! INAM(N,I,J,K) = I,J,K grid에서 N 번째에 해당하는 pID(Phase ID)
! PVAL(N,I,J,K) = I,J,K grid에서 N 번째에 해당하는 phi value
! IRESULT(N,I,J,K) = pppppiiiii 형태로 저장된 수
! CVAL(N,I,J,K) = I,J,K grid의 N번째 component의 c value
!
! "pfm_fort_BinFileMgr.f90": 210707 Adapted from "pfm_FortBinIO151208a.f90"
!-------------------------------------------------------------------------------
! Comments in "pfm_FortBinIO151208a.f90"
! 151208: OPEN format 변경
!    FORM='BINARY':ifort 전용 --> ACCESS='STREAM', FORM='UNFORMATTED': ifort, gfortran 공통
!    (Ref: Binary Format in gfortran, https://gcc.gnu.org/ml/fortran/2013-04/msg00259.html)
!===============================================================================


IMPLICIT INTEGER (I-N)
IMPLICIT DOUBLE PRECISION (A-H, O-Z)


PARAMETER (NNP=15)
INTEGER*1 IICHECK !---> 주의! 1BYTE로 설정
REAL*4 C0_MEAN
INTEGER*4 , ALLOCATABLE, DIMENSION(:,:,:) :: NCOEXIST
INTEGER*4 , ALLOCATABLE, DIMENSION(:,:,:,:) :: INAM, IRESULT
REAL*4 , ALLOCATABLE, DIMENSION(:,:,:,:) :: PVAL
REAL*4 , ALLOCATABLE, DIMENSION(:,:,:,:) :: CVAL



!!배열용 메모리 dynamic allocation <<-------------------------------------------
! system 의 grid 개수 확인 후 배열할당
!!! OPEN(20, FILE='w_c100.out.bin', FORM='BINARY', STATUS='OLD') !for ifort
OPEN(20, FILE='w_c100.out.bin', ACCESS='STREAM', FORM='UNFORMATTED', STATUS='OLD') !for ifort and gfortran


READ(20) IHEAD1, IHEAD2, IHEAD3, INT1, INT2, IM, JM, KM
READ(20) NCOMP

ALLOCATE (NCOEXIST(IM,JM,KM))
ALLOCATE (INAM(NNP,IM,JM,KM))
ALLOCATE (IRESULT(NNP,IM,JM,KM))
ALLOCATE (PVAL(NNP,IM,JM,KM))
ALLOCATE (CVAL(NCOMP,IM,JM,KM))

CLOSE(20)
!!배열용 메모리 dynamic allocation >>-------------------------------------------



!!READ BINARY FILE, phi field <<------------------------------------------------
!!! OPEN(20, FILE='phi100.out.bin', FORM='BINARY', STATUS='OLD') !for ifort
OPEN(20, FILE='phi100.out.bin', ACCESS='STREAM', FORM='UNFORMATTED', STATUS='OLD') !for ifort and gfortran

READ(20) IHEAD1, IHEAD2, IHEAD3, INT1, INT2, IM, JM, KM

DO I=1, IM
DO J=1, JM
DO K=1, KM
 READ(20) IICHECK, (IRESULT(N,I,J,K), N=1,IICHECK)
 NCOEXIST(I,J,K) = INT(IICHECK)
 DO N=1, IICHECK
  INAM(N,I,J,K)=IRESULT(N,I,J,K)-(IRESULT(N,I,J,K)/100000)*100000
  PVAL(N,I,J,K)=FLOAT(IRESULT(N,I,J,K)/100000)/10000.
 END DO
END DO
END DO
END DO

CLOSE(20)
!!READ BINARY FILE, phi field >>------------------------------------------------



!!READ BINARY FILE, c field <<--------------------------------------------------
!!! OPEN (20, FILE='w_c100.out.bin', FORM='BINARY', STATUS='OLD') !for ifort
OPEN (20, FILE='w_c100.out.bin', ACCESS='STREAM', FORM='UNFORMATTED', STATUS='OLD') !for ifort and gfortran

READ(20) IHEAD1, IHEAD2, IHEAD3, INT1, INT2, IM, JM, KM
READ(20) NCOMP

DO N=1, NCOMP
  READ(20) C0_MEAN
END DO

DO I=1, IM
DO J=1, JM
DO K=1, KM
 READ(20) (CVAL(N,I,J,K), N=1,NCOMP)
END DO
END DO
END DO

CLOSE(20)
!!READ BINARY FILE, c field >>--------------------------------------------------



WRITE(*,*) 'Data read from the binary file.'
WRITE(*,*) 'IHEAD1, IHEAD2, IHEAD3 :', IHEAD1, IHEAD2, IHEAD3
WRITE(*,*) 'IM, JM, KM :', IM, JM, KM
WRITE(*,*) 'NCOMP :', NCOMP



!!WRITE BINARY FILE, phi field <<-----------------------------------------------
!!! OPEN (10, FILE='tmp_phi100.out.bin', FORM='BINARY', STATUS='UNKNOWN') !for ifort
OPEN (10, FILE='tmp_phi100.out.bin', ACCESS='STREAM', FORM='UNFORMATTED', STATUS='UNKNOWN') !for ifort and gfortran

WRITE(10) IHEAD1, IHEAD2, IHEAD3, INT1, INT2, IM, JM, KM

DO I=1, IM
DO J=1, JM
DO K=1, KM
 IICHECK = NCOEXIST(I,J,K)
 WRITE(10) IICHECK, ((INT(PVAL(N,I,J,K)*10000)*100000+INAM(N,I,J,K)), N=1,IICHECK)
END DO
END DO
END DO

CLOSE(10)
!!WRITE BINARY FILE, phi field >>-----------------------------------------------



!!WRITE BINARY FILE, c field <<-------------------------------------------------
!!! OPEN (10, FILE='tmp_c100.out.bin', FORM='BINARY', STATUS='UNKNOWN') !for ifort
OPEN (10, FILE='tmp_c100.out.bin', ACCESS='STREAM', FORM='UNFORMATTED', STATUS='UNKNOWN') !for ifort and gfortran

WRITE(10) IHEAD1, IHEAD2, IHEAD3, INT1, INT2, IM, JM, KM

WRITE(10) NCOMP

DO N=1, NCOMP
  WRITE(10) C0_MEAN
END DO

DO I=1, IM
DO J=1, JM
DO K=1, KM
 WRITE(10) (CVAL(N,I,J,K), N=1,NCOMP)
END DO
END DO
END DO

CLOSE(10)
!!WRITE BINARY FILE, c field >>-------------------------------------------------



STOP
END
