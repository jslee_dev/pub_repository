================================================================================
A public repository for SW release
================================================================================

Jae Sang Lee, Since 2019.12.09

|

History
================================================================================

- 2021.08.16: Add ww_*_redist_intel64_*.msi (for installing runtime dll's, if necessary)
- 2021.07.12: Add pfm_gg_view2_Release0707b.zip
- 2020.10.15: Add pfm_gg_view2_Release***.zip
- 2020.02.26: Add IntelSWTools_2019.1.144_redist.zip (Replaced by ww_*_redist_intel64_*.msi)
- 2020.02.17: Rev. CryOri_Release-0.9.200211*.zip
- 2020.02.12: Add CryOri_Release-0.9.200211a.zip
- 2019.12.09: Init. repository
